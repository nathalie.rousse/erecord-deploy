#!/usr/bin/env python
# coding: utf-8

#-----------------------------------------------------------------
__author__    = "Nathalie Rousse (nathalie.rousse@inrae.fr)"
__copyright__ = "Copyright (C) 2023, Inrae (https://www.inrae.fr)"
__license__   = "MIT"
#-----------------------------------------------------------------

###############################################################################
# Use Case of tool information
#
# Using Bioblend library through siwaa.py
#
###############################################################################

###############################################################################
#
#                        INITIALISATION
#
###############################################################################

V = True # True
Vmain = True

from pprint import pprint
import siwaa # using bioblend

CALL_get_tool_id_name_dict =     False # True,False
CALL_for_redelacSticsSimulator = True  # True,False
CALL_for_erecord_file =          False # True,False

###############################################################################
#
#                           METHODS
#
###############################################################################

def get_tool_id_name_dict(sc):
    """Returns dict with key:tool id and value:tool name, for all tools"""

    tools = sc.tc.get_tools()
    tool_id_name_dict = dict()
    for tool in tools:
        tool_id_name_dict[ tool['id'] ] = tool['name']
    return tool_id_name_dict

def get_info_inout(sc, tool_id):  
    """Returns information about tool inputs and outputs"""

    if sc.V: print("\n************ TOOL: '", tool_id, "' ***************\n")

    (inputs_indices, outputs_indices) = sc.tool_ident_inout(tool_id)

    #pprint(inputs_indices)
    #pprint(outputs_indices)

    ret_show = sc.tc.show_tool(tool_id=tool_id, io_details=True)

    if sc.V: print("ret_show keys:", ret_show.keys())

    if V: print("\n************ INPUTS:")

    ret_show_inputs = ret_show['inputs']
    #pprint(ret_show_inputs)
    for i,e in enumerate(ret_show_inputs):
        e_keys = e.keys()
        print("\n*** input indice:", i)
        for k in ['name', 'type', 'optional', 'multiple', 'extensions',
                  'label', 'help']:
            if k in e_keys:
                print("  - input '",k,"':", e[k])
        if e['type'] == 'conditional':
            print("  - Case of input 'type': conditional => has 'cases' key:")
            #print("  - e['cases'] list :", e['cases'])
            cases = e['cases']
            for c,case in enumerate(cases):
                print("    - case indice:", c)
                case_keys = case.keys()
                print("      - case keys:", case.keys())
                if 'inputs' in case_keys:
                    case_inputs = case['inputs']
                    print("      - case 'inputs' (list):", case_inputs)
                    for j,input in enumerate(case_inputs):
                        print("        - input indice:", j)
                        input_keys = input.keys()
                        print("          - input keys:", input_keys)
                        for k in ['name', 'type', 'optional',
                                  'help', 'label', 'value']:
                            if k in input_keys:
                                print("          - input '",k,"':", input[k])

        print("  - keys:", e.keys())
    
    if V: print("\n************ OUTPUTS:")
    
    ret_show_outputs = ret_show['outputs']
    #pprint(ret_show_outputs)
    
    for i,e in enumerate(ret_show_outputs):
        name = e['name']
        print("\n*** output indice:", i)
        print("  - output 'name':", name)
        print("  - output 'output_type':", e['output_type'])
        print("  - output 'format':", e['format'])
        print("  - keys:", e.keys())
        print("  - output 'count':", e['count'])


###############################################################################
#
#               tool_id_name_dict for SERVER tools
#
###############################################################################

if CALL_get_tool_id_name_dict:
    SERVER = 'https://siwaa.toulouse.inrae.fr'
    API_KEY = '9e38ceeedc3c14a215bba53273ff1384'
    sc = siwaa.SiwaaClient(server=SERVER, key=API_KEY, verbose=V)
    if sc.V: print("Server:", SERVER)

    tool_id_name_dict = get_tool_id_name_dict(sc)
    for tool_id in tool_id_name_dict.keys():
        if sc.V: print(tool_id, " : ", tool_id_name_dict[tool_id])

###############################################################################
#
#                   tool named 'redelacSticsSimulator'
#
#       id '.../redelac_stics_s/redelacSticsSimulator/1.0.6'

###############################################################################

if CALL_for_redelacSticsSimulator:
    SERVER = 'https://siwaa.toulouse.inrae.fr'
    API_KEY = '9e38ceeedc3c14a215bba53273ff1384'
    sc = siwaa.SiwaaClient(server=SERVER, key=API_KEY, verbose=V)

    tool_id = 'toolshed-siwaa.toulouse.inrae.fr/repos/siwaa/redelac_stics_s/redelacSticsSimulator/1.0.6'

    get_info_inout(sc=sc, tool_id=tool_id)


###############################################################################
#
#                        'erecord_file' tool
#
###############################################################################

if CALL_for_erecord_file:
    SERVER = 'http://127.0.0.1:9090'
    API_KEY = 'ed1c5cac652311bb01212437d977846f'
    sc = siwaa.SiwaaClient(server=SERVER, key=API_KEY, verbose=V)
    
    tool_id = 'erecord_file'
    
    get_info_inout(sc=sc, tool_id='erecord_file')



