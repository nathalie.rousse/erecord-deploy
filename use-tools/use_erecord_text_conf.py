#!/usr/bin/env python
# coding: utf-8

#-----------------------------------------------------------------
__author__    = "Nathalie Rousse (nathalie.rousse@inrae.fr)"
__copyright__ = "Copyright (C) 2023, Inrae (https://www.inrae.fr)"
__license__   = "MIT"
#-----------------------------------------------------------------

###############################################################################
#
# CONFIGURATION for Use Case of calling 'erecord_text' tool
#
# This file values must be updated according to the use case.
# Into the following code, instructions for that begin with "#>"
#
# - input_text : is a string in memory
#
# Choices among several cases :
# - output_json : data downloaded as a local file, in memory
#
###############################################################################

import json

###############################################################################
#
#                        INITIALISATION
#
###############################################################################

#------------------------------------------------------------------------------
#> Choose SERVER, update API_KEY

SERVER = 'http://127.0.0.1:9090' # local (planemo serve)
API_KEY = 'c782da62332e0260c752d58ea561d1d9' # must be an available value !!!

#SERVER = 'https://siwaa.toulouse.inrae.fr' # Siwaa
#API_KEY = '2e0878d5c7a090e0c517d6225961943d' # must be an available value !!!

#SERVER = 'https://147.100.201.45.nip.io' # Siwaa test (no SSL certificate)
#API_KEY = 'd41b016dc7200ca8c07c570a9785772e' # must be an available value !!!

#------------------------------------------------------------------------------

#> Choose 'print' levels
V = False # True

#> Define Output files folder
OUTPUTS_PATH = "OUTPUTS"  # must exist !!!

# History ---------------------------------------------------------------------

HISTORY_CASE,HISTORY_ID,HISTORY_NAME = None,None,None # Default (don't modify)

#> Choose (comment/uncomment) and define one case

# Case of existing history
#HISTORY_CASE = "EXISTING_HISTORY"
#HISTORY_ID = '9ee2178e4589928f' # must be an available value !!!

# Case of history to be created
HISTORY_CASE = "CREATE_HISTORY"
HISTORY_NAME = "CAS erecord_text"
#HISTORY_NAME = "Use Case of 'erecord_text' tool by Bioblend+siwaa_erecord_text" # (defining HISTORY_NAME is optional)

###############################################################################
#
#                  'erecord_text' TOOL INPUTS
#
###############################################################################

#------------------------------------------------------------------------------
#
# 'input_text' input (optional)
#
# NB : 'input_text' data must contain 'action' value 
#
#> Choose (comment/uncomment) and define one case
#
#------------------------------------------------------------------------------

# INPUT_JSON_JSONDATA must be an available value !!!

# Note : Same INPUT_JSON_JSONDATA as for 'erecord_json' tool and
# then INPUT_JSON_TEXT = json.dumps(INPUT_JSON_JSONDATA)

# Some data examples

#INPUT_JSON_JSONDATA = {"action":"help"}
#INPUT_JSON_JSONDATA = {"action":"get_pkg_data_list", "pkgname":"wwdm"}
#INPUT_JSON_JSONDATA = {"action":"get_pkg_list"}
#INPUT_JSON_JSONDATA = {"action":"get_pkg_vpz_list", "pkgname":"erecord"}
#INPUT_JSON_JSONDATA = {"action":"get_pkg_vpz_list", "pkgname":"wwdm"}

#INPUT_JSON_JSONDATA = {"action":"get_vpz_input",
#      "pkgname":"wwdm", "vpzname":"wwdm.vpz", "style":"tree",
#      "parselect":"cond_meteo.meteo_file" }

# or with "action" value "get_vpz_output", "get_vpz_input", "get_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action":"get_vpz_output",
#                       "pkgname":"wwdm", "vpzname":"wwdm.vpz"}

# or with "action" value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action":"post_vpz_inout",
#      "pkgname":"wwdm", "vpzname":"wwdm.vpz", "duration":20.0}

# or with "action" value "post_vpz_output", "post_vpz_inout" :
INPUT_JSON_JSONDATA = {"action": "post_vpz_output", 
      "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist", 
      "plan":"linear", "restype" : "matrix",
      "cond_wwdm.A": [0.0063, 0.0065, 0.0067], 
      "cond_wwdm.B": 0.00201, "duration":6.0,
      "outselect": "view" }

# or with "action" value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action": "post_vpz_output", 
#      "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist", 
#      "plan": "linear", "restype": "matrix",
#      "cond_meteo.meteo_file": ["31035002.csv", "31035002_bis.csv"],
#      "datafoldercopy" : "overwrite", "duration": 6.0,
#      "outselect": "view.top:wwdm.LAI" }

# or with "action" value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action":"post_vpz_output",
#      "pkgname":"wwdm", "vpzname":"wwdm.vpz",
#      "duration":9.0, "datafoldercopy":"overwrite"}

# or with "action" value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action":"post_vpz_output",
#      "pkgname":"wwdm", "vpzname":"wwdm.vpz",
#      "duration":100.0, "datafoldercopy":"replace"}

INPUT_JSON_TEXT = json.dumps(INPUT_JSON_JSONDATA)

#------------------------------------------------------------------------------
#
# 'datafolder' input (optional)
#
#------------------------------------------------------------------------------
DATAFOLDER_CASE,DATAFOLDER_ID,DATAFOLDER_FILEPATH = None,None,None # Default (don't modify)

# NB : 'datafolder' taken into account only with 'action' values :
# 'post_vpz_output', 'post_vpz_inout'

#> Choose (comment/uncomment) and define one case

# Case of existing dataset ----------------------------------------------------

#DATAFOLDER_CASE = "EXISTING_DATASET"
#DATAFOLDER_ID = 'd6c7433da961816b' # must be an available value !!!

# Case of uploading a local file ----------------------------------------------

#DATAFOLDER_CASE = "UPLOAD_FILE"

# DATAFOLDER_FILEPATH must be an available value !!!
# Some files examples
#DATAFOLDER_FILEPATH = '../data/data.zip'

###############################################################################
#
#              'erecord_text' TOOL OUTPUT
#
# Output is : 'output_json' 
#
# Cases :
# - 'output_json' : download in memory, download as file
#
#> Choose how to get outputs (True/False), several cases possible
#
###############################################################################

# 'output_json' case of downloading in memory
OUTPUT_JSON_DOWNLOAD_IN_MEMORY_CASE = True # values : True,False

# 'output_json' case of downloading as file
OUTPUT_JSON_DOWNLOAD_FILE_CASE = True # values : True,False

###############################################################################

