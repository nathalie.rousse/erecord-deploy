# Using tools erecord_json and erecord_file

Galaxy tools from '[galaxy-tools](../galaxy-tools)' folder.

Calling tools from CLI by curl, Python program, R program.

# Environment for Python program using bioblend or requests libraries

- Install _venv_bioblend Python virtualenv :
```
cd _fab
pip3 install --upgrade pip wheel setuptools virtualenv
python3 -m venv _venv_bioblend
source _venv_bioblend/bin/activate
python3 -m pip  install --upgrade pip wheel setuptools
python3 -m pip install bioblend # to be able to use bioblend
python3 -m pip install requests # to be able to use requests
```

- Use _venv_bioblend :
```
source _fab/_venv_bioblend/bin/activate
python
# or to run program.py command : python program.py
```

