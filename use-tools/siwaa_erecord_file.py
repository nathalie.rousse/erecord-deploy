# -*- coding: UTF-8 -*-
"""
    siwaa_erecord_file
"""

#import bioblend.galaxy
#import json
from pprint import pprint
import siwaa

PM = "[siwaa_erecord_file]"

class Siwaa_erecord_file(object):
    """Specific to 'erecord_file' tool"""

    def __init__(self, sc):
        self.sc = sc # SiwaaClient
        self.tool_id = 'erecord_file'
        if self.sc.V:
            print("Siwaa_erecord_file: Tool", self.tool_id)

    def set_tool_inputs__Vb(self, action_name, input_json_id,
                            datafolder_id, experimentfile_id):
        """Define and return tool_inputs (parameter for run_tool)"""

        # cf .xml <inputs>

        from bioblend.galaxy.tools.inputs import conditional
        from bioblend.galaxy.tools.inputs import dataset
        from bioblend.galaxy.tools.inputs import inputs
        #from bioblend.galaxy.tools.inputs import repeat

        if action_name is None :
            action_name = 'help' # default
        t = inputs()

        # input_json
        if input_json_id is not None:
            t.set("input_json", dataset(input_json_id))

        cond_request_condition = conditional()

        # action_name
        cond_request_condition.set("action_name", action_name)

        # datafolder
        if action_name in ['post_vpz_report', 'post_vpz_output',
                           'post_vpz_inout', 'post_vpz_experiment']:
            if datafolder_id is not None:
                cond_request_condition.set("datafolder",
                                           dataset(datafolder_id))
        # experimentfile
        if action_name == 'post_vpz_experiment':
            if experimentfile_id is not None:
                cond_request_condition.set("experimentfile",
                                           dataset(experimentfile_id))

        t.set("request_condition", cond_request_condition)
        tool_inputs = t.to_dict()
        return tool_inputs

    def set_tool_inputs(self, action_name, input_json_id,
                        datafolder_id, experimentfile_id):
        """Define and return tool_inputs (parameter for run_tool)"""

        # cf .xml <inputs>

        if action_name is None :
            action_name = 'help' # default
        tool_inputs = dict()

        # input_json
        if input_json_id is not None:
            tool_inputs['input_json'] = {'id':input_json_id, 'src':'hda'}

        # action_name
        tool_inputs['request_condition|action_name'] = action_name

        # datafolder
        if action_name in ['post_vpz_report', 'post_vpz_output',
                           'post_vpz_inout', 'post_vpz_experiment']:
            if datafolder_id is not None:
                v = {'id':datafolder_id, 'src':'hda'}
                tool_inputs['request_condition|datafolder'] = v

        # experimentfile
        if action_name == 'post_vpz_experiment':
            if experimentfile_id is not None:
                v = {'id':experimentfile_id, 'src':'hda'}
                tool_inputs['request_condition|experimentfile'] = v

        return tool_inputs

    def run_tool(self, action_name=None, input_json_id=None,
                 datafolder_id=None, experimentfile_id=None):
        """Specific to 'erecord_file' tool

        From erecord_file.xml :

        * inputs:
          - name='action_name'                type='select'
          - name='input_json' optional='true' type='data' format='json'
          - name='datafolder' optional='true' type='data' format='zip'
            if 'action_name' in: 'post_vpz_experiment', 'post_vpz_report',
                                 'post_vpz_output', 'post_vpz_inout'
          - name='experimentfile'             type='data' format='csv,xls'
            if 'action_name' == 'post_vpz_experiment' (mandatory)
  
        * outputs:
          - name='output_json' format='json'
          - name='experiment'  format='xls'
            if 'action_name' in: 'get_vpz_experiment', 'post_vpz_experiment'
          - name='conditions'  format='xls'
            if 'action_name' in: 'get_vpz_report_conditions',
                                 'post_vpz_report_conditions'
          - name='report'      format='zip'
            if 'action_name' in: 'get_vpz_report', 'post_vpz_report'
        """
        #tool_inputs = self.set_tool_inputs__Vb(action_name=action_name,
        tool_inputs = self.set_tool_inputs(action_name=action_name,
                                           input_json_id=input_json_id,
                                           datafolder_id=datafolder_id,
                                           experimentfile_id=experimentfile_id)
        if self.sc.V:
            print("tool_inputs:")
            pprint(tool_inputs)
            print("run_tool with")
            print("'action_name' value :", action_name)
            print("'input_json' Dataset id :", input_json_id)
            print("'datafolder' Dataset id :", datafolder_id)
            print("'experimentfile' Dataset id :", experimentfile_id)
            print("...")

        ret = self.sc.tc.run_tool(history_id=self.sc.history_id,
                                  tool_id=self.tool_id,
                                  tool_inputs=tool_inputs)
        outputs_ids_dict = self.sc.get_outputs_ids(ret_run_tool=ret)
        return outputs_ids_dict

