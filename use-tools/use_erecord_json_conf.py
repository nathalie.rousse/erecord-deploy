#!/usr/bin/env python
# coding: utf-8

#-----------------------------------------------------------------
__author__    = "Nathalie Rousse (nathalie.rousse@inrae.fr)"
__copyright__ = "Copyright (C) 2023, Inrae (https://www.inrae.fr)"
__license__   = "MIT"
#-----------------------------------------------------------------

###############################################################################
#
# CONFIGURATION for Use Case of calling 'erecord_json' tool
#
# This file values must be updated according to the use case.
# Into the following code, instructions for that begin with "#>"
#
# Choices among several cases :
# - input_json : uploading a local file, from memory a string data
# - output_json : data downloaded as a local file, in memory
#
###############################################################################

###############################################################################
#
#                        INITIALISATION
#
###############################################################################

#------------------------------------------------------------------------------
#> Choose SERVER, update API_KEY

SERVER = 'http://127.0.0.1:9090' # local (planemo serve)
API_KEY = '7c8914988829d6a37af668e0b3878c83' # must be an available value !!!

#SERVER = 'https://siwaa.toulouse.inrae.fr' # Siwaa
#API_KEY = '2e0878d5c7a090e0c517d6225961943d' # must be an available value !!!

#SERVER = 'https://147.100.201.45.nip.io' # Siwaa test (no SSL certificate)
#API_KEY = 'cf02d46ed0cd0c5e7767704999f8011e' # must be an available value !!!

#------------------------------------------------------------------------------

#> Choose 'print' levels
V = False # True

#> Define Output files folder
OUTPUTS_PATH = "OUTPUTS"  # must exist !!!

# History ---------------------------------------------------------------------

HISTORY_CASE,HISTORY_ID,HISTORY_NAME = None,None,None # Default (don't modify)

#> Choose (comment/uncomment) and define one case

# Case of existing history
#HISTORY_CASE = "EXISTING_HISTORY"
#HISTORY_ID = '9ee2178e4589928f' # must be an available value !!!

# Case of history to be created
HISTORY_CASE = "CREATE_HISTORY"
HISTORY_NAME = "CAS erecord_json"
#HISTORY_NAME = "Use Case of 'erecord_json' tool by Bioblend+siwaa_erecord_json" # (defining HISTORY_NAME is optional)

###############################################################################
#
#                  'erecord_json' TOOL INPUTS
#
###############################################################################

#------------------------------------------------------------------------------
#
# 'input_json' input (optional)
#
# NB : 'input_json' data must contain 'action' value 
#
#> Choose (comment/uncomment) and define one case
#
#------------------------------------------------------------------------------
INPUT_JSON_CASE,INPUT_JSON_ID,INPUT_JSON_FILEPATH,INPUT_JSON_JSONDATA = None,None,None,None # Default (don't modify)

# Case of existing dataset ----------------------------------------------------

#INPUT_JSON_CASE = "EXISTING_DATASET"
#INPUT_JSON_ID = 'dc3380b8fadf526a' # must be an available value !!!

# Case of uploading a local file ----------------------------------------------

#INPUT_JSON_CASE = "UPLOAD_FILE"

# INPUT_JSON_FILEPATH must be an available value !!!

# Some files examples
#INPUT_JSON_FILEPATH = '../data/data__get_pkg_data_list.json'
#INPUT_JSON_FILEPATH = '../data/data__get_pkg_list.json'
#INPUT_JSON_FILEPATH = '../data/data__get_pkg_vpz_list_2.json'
#INPUT_JSON_FILEPATH = '../data/data__get_pkg_vpz_list.json'
#INPUT_JSON_FILEPATH = '../data/data__get_vpz_input.json'
#INPUT_JSON_FILEPATH = '../data/data__get_vpz_output.json'
#INPUT_JSON_FILEPATH = '../data/data__help.json'
#INPUT_JSON_FILEPATH = '../data/data__post_vpz_inout.json'
INPUT_JSON_FILEPATH = '../data/data__post_vpz_output_2.json'
#INPUT_JSON_FILEPATH = '../data/data__post_vpz_output_3.json'

# Case of uploading from memory a string data ---------------------------------

INPUT_JSON_CASE = "UPLOAD_FROM_MEMORY"

# INPUT_JSON_JSONDATA must be an available value !!!

# Some data examples

#INPUT_JSON_JSONDATA = {"action":"help"}
#INPUT_JSON_JSONDATA = {"action":"get_pkg_data_list", "pkgname":"wwdm"}
#INPUT_JSON_JSONDATA = {"action":"get_pkg_list"}
#INPUT_JSON_JSONDATA = {"action":"get_pkg_vpz_list", "pkgname":"erecord"}
#INPUT_JSON_JSONDATA = {"action":"get_pkg_vpz_list", "pkgname":"wwdm"}

#INPUT_JSON_JSONDATA = {"action":"get_vpz_input",
#      "pkgname":"wwdm", "vpzname":"wwdm.vpz", "style":"tree",
#      "parselect":"cond_meteo.meteo_file" }

# or with "action" value "get_vpz_output", "get_vpz_input", "get_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action":"get_vpz_output",
#                       "pkgname":"wwdm", "vpzname":"wwdm.vpz"}

# or with "action" value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action":"post_vpz_inout",
#      "pkgname":"wwdm", "vpzname":"wwdm.vpz", "duration":20.0}

# or with "action" value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action": "post_vpz_output", 
#      "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist", 
#      "plan":"linear", "restype" : "matrix",
#      "cond_wwdm.A": [0.0063, 0.0065, 0.0067], 
#      "cond_wwdm.B": 0.00201, "duration":6.0,
#      "outselect": "view" }

# or with "action" value "post_vpz_output", "post_vpz_inout" :
INPUT_JSON_JSONDATA = {"action": "post_vpz_output", 
      "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist", 
      "plan": "linear", "restype": "matrix",
      "cond_meteo.meteo_file": ["31035002.csv", "31035002_bis.csv"],
      "datafoldercopy" : "overwrite", "duration": 6.0,
      "outselect": "view.top:wwdm.LAI" }

# or with "action" value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action":"post_vpz_output",
#      "pkgname":"wwdm", "vpzname":"wwdm.vpz",
#      "duration":9.0, "datafoldercopy":"overwrite"}

# or with "action" value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action":"post_vpz_output",
#      "pkgname":"wwdm", "vpzname":"wwdm.vpz",
#      "duration":100.0, "datafoldercopy":"replace"}

#------------------------------------------------------------------------------
#
# 'datafolder' input (optional)
#
#------------------------------------------------------------------------------
DATAFOLDER_CASE,DATAFOLDER_ID,DATAFOLDER_FILEPATH = None,None,None # Default (don't modify)

# NB : 'datafolder' taken into account only with 'action' values :
# 'post_vpz_output', 'post_vpz_inout'

#> Choose (comment/uncomment) and define one case

# Case of existing dataset ----------------------------------------------------

#DATAFOLDER_CASE = "EXISTING_DATASET"
#DATAFOLDER_ID = 'd6c7433da961816b' # must be an available value !!!

# Case of uploading a local file ----------------------------------------------

DATAFOLDER_CASE = "UPLOAD_FILE"

# DATAFOLDER_FILEPATH must be an available value !!!
# Some files examples
DATAFOLDER_FILEPATH = '../data/data.zip'

###############################################################################
#
#              'erecord_json' TOOL OUTPUT
#
# Output is : 'output_json' 
#
# Cases :
# - 'output_json' : download in memory, download as file
#
#> Choose how to get outputs (True/False), several cases possible
#
###############################################################################

# 'output_json' case of downloading in memory
OUTPUT_JSON_DOWNLOAD_IN_MEMORY_CASE = True # values : True,False

# 'output_json' case of downloading as file
OUTPUT_JSON_DOWNLOAD_FILE_CASE = True # values : True,False

###############################################################################

