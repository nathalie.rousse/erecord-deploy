#!/usr/bin/env python
# coding: utf-8

#-----------------------------------------------------------------
__author__    = "Nathalie Rousse (nathalie.rousse@inrae.fr)"
__copyright__ = "Copyright (C) 2023, Inrae (https://www.inrae.fr)"
__license__   = "MIT"
#-----------------------------------------------------------------

###############################################################################
#
# USE CASE of calling 'erecord_file' tool
#
# The use_erecord_file_conf.py file contains the use case CONFIGURATION
#
#       => To define Use Case, modify use_erecord_file_conf.py
#
# Using Bioblend library through siwaa.py
#
# Several cases :
# - input_json uploading a local file, from memory a string data
# - output_json data downloaded as a local file, in memory
#
###############################################################################

###############################################################################
#
#                        INITIALISATION
#
###############################################################################

from pprint import pprint
import siwaa # using bioblend
import siwaa_erecord_file
import use_erecord_file_conf as CONF

Vmain = True # True,False
sc = siwaa.SiwaaClient(server=CONF.SERVER, key=CONF.API_KEY, verbose=CONF.V)

# History
history_case = CONF.HISTORY_CASE
if history_case == "EXISTING_HISTORY" :
    if Vmain : print("[main erecord_file] Case of existing history")
    history_id = CONF.HISTORY_ID
    history_id = sc.select_history(history_id=history_id)
else:
    if Vmain : print("[main erecord_file] Case of history to be created")
    history_name = CONF.HISTORY_NAME
    if history_name is not None:
        history_id = sc.create_history(name=history_name)
    else:
        history_id = sc.create_history()

###############################################################################
#
#                  'erecord_file' TOOL INPUTS
#
###############################################################################

# Input 'action_name'
action_name = CONF.ACTION_NAME

# 'input_json' input (optional)
input_json_case = CONF.INPUT_JSON_CASE
if input_json_case == "EXISTING_DATASET":
    if Vmain : print("[main erecord_file] 'input_json' input:",
                     "Case of Existing dataset")
    hda_input_json_id = CONF.INPUT_JSON_ID
elif input_json_case == "UPLOAD_FILE":
    if Vmain : print("[main erecord_file] 'input_json' input:",
                     "Case of Uploading a local file")
    hda_input_json_id = sc.upload_file(path=CONF.INPUT_JSON_FILEPATH)
elif input_json_case == "UPLOAD_FROM_MEMORY":
    if Vmain : print("[main erecord_file] 'input_json' input:",
                     "Case of Uploading from memory a string data")
    data_input_json = CONF.INPUT_JSON_JSONDATA
    hda_input_json_id = sc.upload_json_data(json_data=data_input_json,
                                        file_name="data_input_json.json")
else: # None
    if Vmain : print("[main erecord_file] 'input_json' input, Case of none")
    hda_input_json_id = None

# 'datafolder' input (optional)

# 'datafolder' taken into account only with action_name values :
# 'post_vpz_experiment', 'post_vpz_report', 'post_vpz_output', 'post_vpz_inout'
maybe_datafolder = False
if action_name in ['post_vpz_experiment', 'post_vpz_report',
                   'post_vpz_output', 'post_vpz_inout']:
    maybe_datafolder = True

datafolder_case = CONF.DATAFOLDER_CASE
if datafolder_case == "EXISTING_DATASET" and maybe_datafolder:
    if Vmain : print("[main erecord_file] 'datafolder' input:",
                     "Case of Existing dataset")
    hda_datafolder_id = CONF.DATAFOLDER_ID
elif datafolder_case == "UPLOAD_FILE" and maybe_datafolder:
    if Vmain : print("[main erecord_file] 'datafolder' input:",
                     "Case of Uploading a local file")
    hda_datafolder_id = sc.upload_file(path=CONF.DATAFOLDER_FILEPATH,
                             file_type='zip', file_name="data_datafolder.zip")
else: # None
    if Vmain : print("[main erecord_file] 'datafolder' input: Case of none")
    hda_datafolder_id = None

# 'experimentfile' input

# 'experimentfile' taken into account and mandatory with action_name value :
# 'post_vpz_experiment'
required_experimentfile = False
if action_name == 'post_vpz_experiment':
    required_experimentfile = True

experimentfile_case = CONF.EXPERIMENTFILE_CASE

if experimentfile_case == "EXISTING_DATASET" and required_experimentfile:
    if Vmain : print("[main erecord_file] 'experimentfile' input:",
                     "Case of Existing dataset")
    hda_experimentfile_id = CONF.EXPERIMENTFILE_ID
elif experimentfile_case == "UPLOAD_FILE" and required_experimentfile:
    if Vmain : print("[main erecord_file] 'experimentfile' input:",
                     "Case of Uploading a local file")
    hda_experimentfile_id = sc.upload_file(path=CONF.EXPERIMENTFILE_FILEPATH,
                                           file_type='xls',
                                           file_name="data_experimentfile.xls")
else: # None
    if Vmain : print("[main erecord_file] 'experimentfile' input:",
                     "Case of none")
    hda_experimentfile_id = None

###############################################################################
#
#           RUN 'erecord_file' TOOL (tool_id = 'erecord_file')
#
###############################################################################

if Vmain : print("[main erecord_file] Run 'erecord_file' tool")

erecord_file = siwaa_erecord_file.Siwaa_erecord_file(sc)

outputs_ids_dict = erecord_file.run_tool(action_name=action_name,
                                      input_json_id=hda_input_json_id,
                                      datafolder_id=hda_datafolder_id,
                                      experimentfile_id=hda_experimentfile_id)

###############################################################################
#
#              'erecord_file' TOOL OUTPUTS
#
# Outputs : 'output_json' 
#         + maybe a second file ('experiment', 'conditions', 'report')
#
# Cases :
# - 'output_json' : download in memory, download as file
# -  second file  : download as file
#
###############################################################################

# 'output_json' output
# outputs_ids_dict['output_json'] is the dataset to be downloaded
if CONF.OUTPUT_JSON_DOWNLOAD_IN_MEMORY_CASE:
                                            # 'output_json' download in memory
    hda_output_json_id = outputs_ids_dict['output_json']
    output_json_data = sc.download_json_data(dataset_id=hda_output_json_id,
                                             maxwait=12000)
    if Vmain:
        print("\n--------------------------------------------------")
        print("[main erecord_file] Download 'output_json' in memory:")
        print("\n=> Request:")
        pprint(output_json_data['data'])
        print("\n=> And response:")
        output_json = output_json_data['response']
        pprint(output_json)
        print("\n--------------------------------------------------")

# Download as file :
# - 'output_json' output (depending on choice output_json_file_case)
# -  2nd output (if exists)
for output_name in outputs_ids_dict.keys():
    if output_name in ['output_json', 'experiment', 'conditions', 'report']: 
        if (output_name=='output_json' and not CONF.OUTPUT_JSON_DOWNLOAD_FILE_CASE) or (output_name!='output_json' and not CONF.SECOND_FILE_DOWNLOAD_FILE_CASE):
            pass
        else: # output_name download as file
            hda_output_id = outputs_ids_dict[output_name]
            output_file_path = sc.download_file(dataset_id=hda_output_id,
                                  output_path=CONF.OUTPUTS_PATH, maxwait=10000)
            if Vmain:
                print("\n--------------------------------------------------")
                print("[main erecord_file] Download '",output_name, "'",
                      "as file:", output_file_path)
                print("\n--------------------------------------------------")

