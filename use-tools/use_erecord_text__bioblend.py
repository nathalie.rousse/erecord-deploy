#!/usr/bin/env python
# coding: utf-8

#-----------------------------------------------------------------
__author__    = "Nathalie Rousse (nathalie.rousse@inrae.fr)"
__copyright__ = "Copyright (C) 2023, Inrae (https://www.inrae.fr)"
__license__   = "MIT"
#-----------------------------------------------------------------

###############################################################################
#
# USE CASE of calling 'erecord_text' tool
#
# The use_erecord_text_conf.py contains the use case CONFIGURATION
#
#       => To define Use Case, modify use_erecord_text_conf.py
#
# Using Bioblend library through siwaa.py
#
# - input_text : is a string in memory
#
# Several cases :
# - output_json data downloaded as a local file, in memory
#
###############################################################################

###############################################################################
#
#                        INITIALISATION
#
###############################################################################

from pprint import pprint
import siwaa # using bioblend
import siwaa_erecord_text
import use_erecord_text_conf as CONF

Vmain = True # True,False
sc = siwaa.SiwaaClient(server=CONF.SERVER, key=CONF.API_KEY, verbose=CONF.V)

# History
history_case = CONF.HISTORY_CASE
if history_case == "EXISTING_HISTORY" :
    if Vmain : print("[main erecord_text] Case of existing history")
    history_id = CONF.HISTORY_ID
    history_id = sc.select_history(history_id=history_id)
else:
    if Vmain : print("[main erecord_text] Case of history to be created")
    history_name = CONF.HISTORY_NAME
    if history_name is not None:
        history_id = sc.create_history(name=history_name)
    else:
        history_id = sc.create_history()

###############################################################################
#
#                  'erecord_text' TOOL INPUTS
#
###############################################################################

# 'input_text' input (optional)
if Vmain : print("[main erecord_text] 'input_text input:",
                 "string from memory")
input_text = CONF.INPUT_JSON_TEXT

# 'datafolder' input (optional)

# 'datafolder' taken into account only with 'action' values :
# 'post_vpz_output', 'post_vpz_inout'
# (not controlled)
datafolder_case = CONF.DATAFOLDER_CASE
if datafolder_case == "EXISTING_DATASET":
    if Vmain : print("[main erecord_text] 'datafolder' input:",
                     "Case of Existing dataset")
    hda_datafolder_id = CONF.DATAFOLDER_ID
elif datafolder_case == "UPLOAD_FILE":
    if Vmain : print("[main erecord_text] 'datafolder' input:",
                     "Case of Uploading a local file")
    hda_datafolder_id = sc.upload_file(path=CONF.DATAFOLDER_FILEPATH,
                             file_type='zip', file_name="data_datafolder.zip")
else: # None
    if Vmain : print("[main erecord_text] 'datafolder' input: Case of none")
    hda_datafolder_id = None

###############################################################################
#
#           RUN 'erecord_text' TOOL (tool_id = 'erecord_text')
#
###############################################################################

if Vmain : print("[main erecord_text] Run 'erecord_text' tool")

erecord_text = siwaa_erecord_text.Siwaa_erecord_text(sc)

hda_output_json_id = erecord_text.run_tool(input_text=input_text,
                                           datafolder_id=hda_datafolder_id)

###############################################################################
#
#              'erecord_text' TOOL OUTPUT
#
# Output : 'output_json' 
#
# Cases :
# - 'output_json' : download in memory, download as file
#
###############################################################################

# 'output_json' output
# hda_output_json_id is the dataset to be downloaded

if CONF.OUTPUT_JSON_DOWNLOAD_IN_MEMORY_CASE: # download in memory
    output_json_data = sc.download_json_data(dataset_id=hda_output_json_id,
                                             maxwait=12000)
    if Vmain:
        print("\n--------------------------------------------------")
        print("[main erecord_text] Download 'output_json' in memory:")
        print("\n=> Request:")
        pprint(output_json_data['data'])
        print("\n=> And response:")
        output_json = output_json_data['response']
        pprint(output_json)
        print("\n--------------------------------------------------")

if CONF.OUTPUT_JSON_DOWNLOAD_FILE_CASE: # download as file
    output_json_file_path = sc.download_file(dataset_id=hda_output_json_id,
                                 output_path=CONF.OUTPUTS_PATH, maxwait=10000)
    if Vmain:
        print("\n--------------------------------------------------")
        print("[main erecord_text] Download 'output_json' as file:",
              output_json_file_path)
        print("\n--------------------------------------------------")

