# -*- coding: UTF-8 -*-
"""
    siwaa_erecord_text
"""

#import bioblend.galaxy
#import json
from pprint import pprint
import siwaa

PM = "[siwaa_erecord_text]"

class Siwaa_erecord_text(object):
    """Specific to 'erecord_text' tool"""

    def __init__(self, sc):

        self.sc = sc # SiwaaClient
        self.tool_id = 'erecord_text'
        if self.sc.V:
            print("Siwaa_erecord_text: Tool", self.tool_id)

    def set_tool_inputs(self, input_text, datafolder_id):
        """Define and return tool_inputs (parameter for run_tool)"""

        # cf .xml <inputs>

        tool_inputs = dict()

        # input_json
        if input_text is not None:
            tool_inputs['input_text'] = input_text

        # datafolder
        if datafolder_id is not None:
            tool_inputs['datafolder'] = {'id':datafolder_id, 'src':'hda'}

        print("ZZZZ tool_inputs:", tool_inputs)
        return tool_inputs

    def run_tool(self, input_text=None, datafolder_id=None):
        """
        From erecord_text.xml :
        * inputs:
          - name='input_text' optional='true' type='text'
          - name='datafolder' optional='true' type='data" format='zip'
        * outputs:
          - name='output_json' format='json'
        """

        tool_inputs = self.set_tool_inputs(input_text=input_text,
                                           datafolder_id=datafolder_id)
        if self.sc.V:
            print("tool_inputs:")
            pprint(tool_inputs)
            print("run_tool with")
            print("'input_text' :", input_text)
            print("'datafolder' Dataset id :", datafolder_id)
            print("...")

        ret = self.sc.tc.run_tool(history_id=self.sc.history_id,
                                  tool_id=self.tool_id,
                                  tool_inputs=tool_inputs)
        hda_output_json_id = self.sc.get_first_output_id(ret_run_tool=ret)
        return hda_output_json_id

