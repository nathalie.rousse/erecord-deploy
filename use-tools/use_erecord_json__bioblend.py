#!/usr/bin/env python
# coding: utf-8

#-----------------------------------------------------------------
__author__    = "Nathalie Rousse (nathalie.rousse@inrae.fr)"
__copyright__ = "Copyright (C) 2023, Inrae (https://www.inrae.fr)"
__license__   = "MIT"
#-----------------------------------------------------------------

###############################################################################
#
# USE CASE of calling 'erecord_json' tool
#
# The use_erecord_json_conf.py contains the use case CONFIGURATION
#
#       => To define Use Case, modify use_erecord_json_conf.py
#
# Using Bioblend library through siwaa.py
#
# Several cases :
# - input_json uploading a local file, from memory a string data
# - output_json data downloaded as a local file, in memory
#
###############################################################################

###############################################################################
#
#                        INITIALISATION
#
###############################################################################

from pprint import pprint
import siwaa # using bioblend
import siwaa_erecord_json
import use_erecord_json_conf as CONF

Vmain = True # True,False
sc = siwaa.SiwaaClient(server=CONF.SERVER, key=CONF.API_KEY, verbose=CONF.V)

# History
history_case = CONF.HISTORY_CASE
if history_case == "EXISTING_HISTORY" :
    if Vmain : print("[main erecord_json] Case of existing history")
    history_id = CONF.HISTORY_ID
    history_id = sc.select_history(history_id=history_id)
else:
    if Vmain : print("[main erecord_json] Case of history to be created")
    history_name = CONF.HISTORY_NAME
    if history_name is not None:
        history_id = sc.create_history(name=history_name)
    else:
        history_id = sc.create_history()

###############################################################################
#
#                  'erecord_json' TOOL INPUTS
#
###############################################################################

# 'input_json' input (optional)
input_json_case = CONF.INPUT_JSON_CASE
if input_json_case == "EXISTING_DATASET":
    if Vmain : print("[main erecord_json] 'input_json' input:",
                     "Case of Existing dataset")
    hda_input_json_id = CONF.INPUT_JSON_ID
elif input_json_case == "UPLOAD_FILE":
    if Vmain : print("[main erecord_json] 'input_json' input:",
                     "Case of Uploading a local file")
    hda_input_json_id = sc.upload_file(path=CONF.INPUT_JSON_FILEPATH)
elif input_json_case == "UPLOAD_FROM_MEMORY":
    if Vmain : print("[main erecord_json] 'input_json' input:",
                     "Case of Uploading from memory a string data")
    data_input_json = CONF.INPUT_JSON_JSONDATA
    hda_input_json_id = sc.upload_json_data(json_data=data_input_json,
                                        file_name="data_input_json.json")
else: # None
    if Vmain : print("[main erecord_json] 'input_json' input, Case of none")
    hda_input_json_id = None

# 'datafolder' input (optional)

# 'datafolder' taken into account only with 'action' values :
# 'post_vpz_output', 'post_vpz_inout'
# (not controlled)
datafolder_case = CONF.DATAFOLDER_CASE
if datafolder_case == "EXISTING_DATASET":
    if Vmain : print("[main erecord_json] 'datafolder' input:",
                     "Case of Existing dataset")
    hda_datafolder_id = CONF.DATAFOLDER_ID
elif datafolder_case == "UPLOAD_FILE":
    if Vmain : print("[main erecord_json] 'datafolder' input:",
                     "Case of Uploading a local file")
    hda_datafolder_id = sc.upload_file(path=CONF.DATAFOLDER_FILEPATH,
                             file_type='zip', file_name="data_datafolder.zip")
else: # None
    if Vmain : print("[main erecord_json] 'datafolder' input: Case of none")
    hda_datafolder_id = None

###############################################################################
#
#           RUN 'erecord_json' TOOL (tool_id = 'erecord_json')
#
###############################################################################

if Vmain : print("[main erecord_json] Run 'erecord_json' tool")

erecord_json = siwaa_erecord_json.Siwaa_erecord_json(sc)

hda_output_json_id = erecord_json.run_tool(input_json_id=hda_input_json_id,
                                           datafolder_id=hda_datafolder_id)

###############################################################################
#
#              'erecord_json' TOOL OUTPUT
#
# Output : 'output_json' 
#
# Cases :
# - 'output_json' : download in memory, download as file
#
###############################################################################

# 'output_json' output
# hda_output_json_id is the dataset to be downloaded

if CONF.OUTPUT_JSON_DOWNLOAD_IN_MEMORY_CASE: # download in memory
    output_json_data = sc.download_json_data(dataset_id=hda_output_json_id,
                                             maxwait=12000)
    if Vmain:
        print("\n--------------------------------------------------")
        print("[main erecord_json] Download 'output_json' in memory:")
        print("\n=> Request:")
        pprint(output_json_data['data'])
        print("\n=> And response:")
        output_json = output_json_data['response']
        pprint(output_json)
        print("\n--------------------------------------------------")

if CONF.OUTPUT_JSON_DOWNLOAD_FILE_CASE: # download as file
    output_json_file_path = sc.download_file(dataset_id=hda_output_json_id,
                                 output_path=CONF.OUTPUTS_PATH, maxwait=10000)
    if Vmain:
        print("\n--------------------------------------------------")
        print("[main erecord_json] Download 'output_json' as file:",
              output_json_file_path)
        print("\n--------------------------------------------------")

