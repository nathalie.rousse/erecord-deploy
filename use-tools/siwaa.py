# -*- coding: UTF-8 -*-
"""
    siwaa

    Case of Siwaa test (no SSL certificate) :
    - add parameter verify=False when calling :
      bioblend.galaxy.GalaxyInstance(url=server, key=key, verify=False)
    - gi.tools.upload_file can not be called/used
      => use request library to upload files 
"""

import bioblend.galaxy
import json

from pprint import pprint

PM = "[siwaa]"

class SiwaaClient(object):
    """To use Siwaa API by using bioblend library

    Note : For Siwaa test (no SSL certificate), requires adaptations
    """

    def __init__(self, server, key, verbose=True):
        """..."""

        self.V = verbose
        self.gi = bioblend.galaxy.GalaxyInstance(url=server, key=key)
        #self.gi = bioblend.galaxy.GalaxyInstance(url=server, key=key,
        #                                         verify=False)
        self.tc = bioblend.galaxy.tools.ToolClient(galaxy_instance=self.gi)
        self.dc = bioblend.galaxy.datasets.DatasetClient(self.gi)

        self.history_id = None  # history

        if self.V:
            print("SiwaaClient for server:", server,
                  ", no history selected for the moment")

    ###########################################################################
    # History 

    def create_history(self, name=None) :
        """..."""

        try :
            if name is None:
                history = self.gi.histories.create_history()
            else:
                history = self.gi.histories.create_history(name=name)
            self.history_id = history["id"]
        except:
            raise
        if self.V:
            print("History created, id:", self.history_id)
        return self.history_id

    def select_history(self, history_id):
        self.history_id = history_id
        if self.V:
            print("History selected, id:", self.history_id)
        return self.history_id

    def get_history_id(self):
        return self.history_id

    ###########################################################################
    # Tool (See also Specific Siwaa_xxx)

    def get_tool_inputs(self, tool_id):
        """get and return initial tool_inputs for the tool (tool_id)
           tool_inputs : parameter for run_tool
           tool_inputs returned depends on default values into tool .xml
        """
        ret_build = self.tc.build(tool_id=tool_id, history_id=self.history_id)
        tool_inputs = ret_build['state_inputs']
        return tool_inputs

    def tool_ident_inout(self, tool_id):
        """Identity of a tool : inputs and outputs (using tc.show_tool)"""

        def get_inputs_indices(ret_show):
            """Build and return inputs_indices (indice according to inputs)"""
            inputs_indices = dict()
            ret_show_inputs = ret_show['inputs']
            for i,e in enumerate(ret_show_inputs):
                name = e['name']
                inputs_indices[name] = i
            return inputs_indices

        def get_outputs_indices(ret_show):
            """Build and return outputs_indices
               (indice according to output datas)
            """
            outputs_indices = dict()
            ret_show_outputs = ret_show['outputs']
            for i,e in enumerate(ret_show_outputs):
                name = e['name']
                #format = e['format']
                #label = e['label']
                #output_type = e['output_type']
                #hidden = e['hidden']
                #count = e['count']
                outputs_indices[name] = i
            return outputs_indices

        ret_show = self.tc.show_tool(tool_id=tool_id, io_details=True)
        inputs_indices = get_inputs_indices(ret_show)
        outputs_indices = get_outputs_indices(ret_show)
        if self.V:
            print("SiwaaClient ident_inout: Tool", tool_id)
            print("inputs_indices:", inputs_indices)
            print("outputs_indices:", outputs_indices)
        return (inputs_indices, outputs_indices)

    def get_outputs_ids(self, ret_run_tool):
        """Build and return outputs_ids (id according to output_name)"""

        hdas_ret = ret_run_tool['outputs']
        outputs_ids_dict = dict()
        for hda_ret in hdas_ret:
            output_name = hda_ret['output_name']
            outputs_ids_dict[output_name] = hda_ret['id']

        if self.V:
            print("=> 'outputs Datasets ident:")
            for hda_ret in hdas_ret:
                print("   - output_name:", hda_ret['output_name'], " : ",
                      "id:", hda_ret['id'], "name:", hda_ret['name'],
                      "file_ext:", hda_ret['file_ext'])
            print("get_outputs_ids returns:", outputs_ids_dict)
        return outputs_ids_dict

    def get_first_output_id(self, ret_run_tool):
        """return id of first output (adapted when only 1 output)"""

        hdas_ret = ret_run_tool['outputs']
        hda_ret = hdas_ret[0] # only 1st output dataset
        hda_output_id = hda_ret['id']
        if self.V:
            print("=> 1st output Dataset id :", hda_output_id)
            print("get_first_output_id returns:", hda_output_id)
        return hda_output_id

    ###########################################################################
    # Datas

    def upload_file(self, path, file_type='auto', file_name=None):
        if file_name is None:
            ret = self.gi.tools.upload_file(history_id=self.history_id,
                                            path=path, file_type=file_type)
        else:
            ret = self.gi.tools.upload_file(history_id=self.history_id,
                                            path=path, file_type=file_type,
                                            file_name=file_name)
        hda_id = ret['outputs'][0]['id']
        if self.V:
            print("Uploaded from local file:", path, ", Dataset id:", hda_id)
        return hda_id


    def upload_json_data(self, json_data, file_name=None):

        str_data = json.dumps(json_data)

        # paste_content : file_type default value 'auto' => json recognized
        if file_name is None:
            ret = self.gi.tools.paste_content(history_id=self.history_id,
                                              content=str_data)
        else:
            ret = self.gi.tools.paste_content(history_id=self.history_id,
                                              content=str_data,
                                              file_name=file_name)
        hda_id = ret['outputs'][0]['id']
        if self.V:
            print("Uploaded from memory:", json_data, ", Dataset id:", hda_id)

        return hda_id

    # Note for download_dataset setting :
    # - It is possible to download a dataset **to file** or **in memory** :
    #     - *download_dataset(dataset_id: str, **file_path: None = None**, use_default_filename: bool = True, require_ok_state: bool = True, maxwait: float = 12000) --> **bytes***
    #     - *download_dataset(dataset_id: str, **file_path: str**, use_default_filename: bool = True, require_ok_state: bool = True, maxwait: float = 12000) --> **str***
    # - Parameter **maxwait** (float) : total time (in seconds) to wait for the dataset state to become terminal. If the dataset state is not terminal within this time, a DatasetTimeoutException will be thrown.
    
    def download_file(self, dataset_id, output_path, maxwait=12000):

        output_file_path = self.dc.download_dataset(dataset_id=dataset_id,
                             file_path=output_path, use_default_filename=True,
                             require_ok_state=True, maxwait=maxwait)
        if self.V:
            print("Dataset (id:", dataset_id, "),",
                  "downloaded as file:", output_file_path)
        return output_file_path

    def download_json_data(self, dataset_id, maxwait=12000):

        bytes_data = self.dc.download_dataset(dataset_id=dataset_id,
                        file_path=None, require_ok_state=True, maxwait=maxwait)

        if self.V:
            print("Dataset (id:", dataset_id, "),",
                  "downloaded in memory:", bytes_data)

        output_json_data = json.loads(bytes_data) # as json data
        if self.V:
            print("\nAs json data :")
            pprint(output_json_data)
        return output_json_data

