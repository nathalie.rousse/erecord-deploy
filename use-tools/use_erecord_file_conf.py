#!/usr/bin/env python
# coding: utf-8

#-----------------------------------------------------------------
__author__    = "Nathalie Rousse (nathalie.rousse@inrae.fr)"
__copyright__ = "Copyright (C) 2023, Inrae (https://www.inrae.fr)"
__license__   = "MIT"
#-----------------------------------------------------------------

###############################################################################
#
# CONFIGURATION for Use Case of calling 'erecord_file' tool
#
# This file values must be updated according to the use case.
# Into the following code, instructions for that begin with "#>"
#
# Choices among several cases :
# - input_json : uploading a local file, from memory a string data
# - output_json : data downloaded as a local file, in memory
#
###############################################################################

###############################################################################
#
#                        INITIALISATION
#
###############################################################################

#------------------------------------------------------------------------------
#> Choose SERVER, update API_KEY

SERVER = 'http://127.0.0.1:9090' # local (planemo serve)
API_KEY = '7c8914988829d6a37af668e0b3878c83' # must be an available value !!!

#SERVER = 'https://siwaa.toulouse.inrae.fr' # Siwaa
#API_KEY = '2e0878d5c7a090e0c517d6225961943d' # must be an available value !!!

#SERVER = 'https://147.100.201.45.nip.io' # Siwaa test (no SSL certificate)
#API_KEY = 'cf02d46ed0cd0c5e7767704999f8011e' # must be an available value !!!

#------------------------------------------------------------------------------

#> Choose 'print' levels
V = False # True

#> Define Output files folder
OUTPUTS_PATH = "OUTPUTS"  # must exist !!!

# History ---------------------------------------------------------------------

HISTORY_CASE,HISTORY_ID,HISTORY_NAME = None,None,None # Default (don't modify)

#> Choose (comment/uncomment) and define one case

# Case of existing history
#HISTORY_CASE = "EXISTING_HISTORY"
#HISTORY_ID = '9ee2178e4589928f' # must be an available value !!!

# Case of history to be created
HISTORY_CASE = "CREATE_HISTORY"
HISTORY_NAME = "CAS erecord_file"
#HISTORY_NAME = "Use Case of 'erecord_file' tool by Bioblend+siwaa_erecord_file" # (defining HISTORY_NAME is optional)

###############################################################################
#
#                  'erecord_file' TOOL INPUTS
#
###############################################################################

#------------------------------------------------------------------------------
#
# Input 'action_name'
#
#> Choose ACTION_NAME value among the following available values
#
#------------------------------------------------------------------------------

#ACTION_NAME = 'help'
#ACTION_NAME = 'get_pkg_list'
#ACTION_NAME = 'get_pkg_vpz_list'
#ACTION_NAME = 'get_pkg_data_list'
#ACTION_NAME = 'get_vpz_input'
#ACTION_NAME = 'post_vpz_input'
#ACTION_NAME = 'get_vpz_output'
#ACTION_NAME = 'post_vpz_output'
#ACTION_NAME = 'get_vpz_inout'
#ACTION_NAME = 'post_vpz_inout'
#ACTION_NAME = 'get_vpz_experiment'
#ACTION_NAME = 'post_vpz_experiment'
#ACTION_NAME = 'get_vpz_report'
ACTION_NAME = 'post_vpz_report'
#ACTION_NAME = 'get_vpz_report_conditions'
#ACTION_NAME = 'post_vpz_report_conditions'

#------------------------------------------------------------------------------
#
# 'input_json' input (optional)
#
# NB : 'action' value that may be included into 'input_json' would be ignored
#
#> Choose (comment/uncomment) and define one case
#
#------------------------------------------------------------------------------
INPUT_JSON_CASE,INPUT_JSON_ID,INPUT_JSON_FILEPATH,INPUT_JSON_JSONDATA = None,None,None,None # Default (don't modify)

# Case of existing dataset ----------------------------------------------------

#INPUT_JSON_CASE = "EXISTING_DATASET"
#INPUT_JSON_ID = 'dc3380b8fadf526a' # must be an available value !!!

# Case of uploading a local file ----------------------------------------------

#INPUT_JSON_CASE = "UPLOAD_FILE"

# INPUT_JSON_FILEPATH must be an available value !!!

# Some files examples
#INPUT_JSON_FILEPATH = '../data/data__vpz.json'
#INPUT_JSON_FILEPATH = '../data/data__get_pkg_data_list.json'
#INPUT_JSON_FILEPATH = '../data/data__get_pkg_list.json'
#INPUT_JSON_FILEPATH = '../data/data__get_pkg_vpz_list_2.json'
#INPUT_JSON_FILEPATH = '../data/data__get_pkg_vpz_list.json'
#INPUT_JSON_FILEPATH = '../data/data__get_vpz_input.json'
#INPUT_JSON_FILEPATH = '../data/data__get_vpz_output.json'
#INPUT_JSON_FILEPATH = '../data/data__help.json'
#INPUT_JSON_FILEPATH = '../data/data__post_vpz_inout.json'
#INPUT_JSON_FILEPATH = '../data/data__post_vpz_output_2.json'
#INPUT_JSON_FILEPATH = '../data/data__post_vpz_output_3.json'
#INPUT_JSON_FILEPATH = '../data/data__post_vpz_experiment.json'
#INPUT_JSON_FILEPATH = '../data/data__post_vpz_report.json'

# Case of uploading from memory a string data ---------------------------------

INPUT_JSON_CASE = "UPLOAD_FROM_MEMORY"

# INPUT_JSON_JSONDATA must be an available value !!!

# Some data examples
#INPUT_JSON_JSONDATA = {}
#INPUT_JSON_JSONDATA = {"pkgname":"wwdm"}
#INPUT_JSON_JSONDATA = {"pkgname":"erecord"}
#INPUT_JSON_JSONDATA = {"pkgname":"wwdm", "vpzname":"wwdm.vpz"}

# with action_name value "get_vpz_input" :
#INPUT_JSON_JSONDATA = {"pkgname":"wwdm", "vpzname":"wwdm.vpz",
#                       "style":"tree",
#                       "parselect":"cond_meteo.meteo_file" }

# with action_name value "get_vpz_output", "get_vpz_input", "get_vpz_inout" :
#INPUT_JSON_JSONDATA = {"action":"get_vpz_output",
#                       "pkgname":"wwdm", "vpzname":"wwdm.vpz"}

# with action_name value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"pkgname":"wwdm", "vpzname":"wwdm.vpz",
#                       "duration":20.0}

# with action_name value "post_vpz_output", "post_vpz_inout" :
#INPUT_JSON_JSONDATA = {"pkgname": "wwdm", "vpzname": "wwdm.vpz",
#      "style": "compactlist", "plan":"linear", "restype" : "matrix",
#      "cond_wwdm.A": [0.0063, 0.0065, 0.0067], 
#      "cond_wwdm.B": 0.00201, "duration":6.0,
#      "outselect": "view" }

# with action_name value "post_vpz_output", "post_vpz_inout" ;
INPUT_JSON_JSONDATA = {"pkgname": "wwdm", "vpzname": "wwdm.vpz",
      "style": "compactlist", "plan": "linear", "restype": "matrix",
      "cond_meteo.meteo_file": ["31035002.csv", "31035002_bis.csv"],
      "datafoldercopy" : "overwrite", "duration": 6.0,
      "outselect": "view.top:wwdm.LAI" }

# with action_name value "post_vpz_report" :
#INPUT_JSON_JSONDATA = {"pkgname":"wwdm", "vpzname":"wwdm.vpz",
#      "plan": "linear", "restype": "dataframe",
#      "parselect": ["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.K", "cond_wwdm.TI"],
#      "cond_wwdm.A": [0.0063, 0.0065, 0.0067],
#      "cond_wwdm.B": 0.00201, "duration": 6.0,
#      "outselect": "all", "report": "all", "mode": "bycol"
#}

# with action_name value "post_vpz_output", "post_vpz_inout" ;
#INPUT_JSON_JSONDATA = {"pkgname":"wwdm", "vpzname":"wwdm.vpz",
#      "duration":9.0, "datafoldercopy":"overwrite"}

# with action_name value "post_vpz_output", "post_vpz_inout" ;
#INPUT_JSON_JSONDATA = {"pkgname":"wwdm", "vpzname":"wwdm.vpz",
#      "duration":100.0, "datafoldercopy":"replace"}

#------------------------------------------------------------------------------
#
# 'datafolder' input (optional)
#
#------------------------------------------------------------------------------
DATAFOLDER_CASE,DATAFOLDER_ID,DATAFOLDER_FILEPATH = None,None,None # Default (don't modify)

# NB : 'datafolder' taken into account only with ACTION_NAME values :
# 'post_vpz_experiment', 'post_vpz_report', 'post_vpz_output', 'post_vpz_inout'

#> Choose (comment/uncomment) and define one case

# Case of existing dataset ----------------------------------------------------

#DATAFOLDER_CASE = "EXISTING_DATASET"
#DATAFOLDER_ID = 'd6c7433da961816b' # must be an available value !!!

# Case of uploading a local file ----------------------------------------------

DATAFOLDER_CASE = "UPLOAD_FILE"

# DATAFOLDER_FILEPATH must be an available value !!!
# Some files examples
DATAFOLDER_FILEPATH = '../data/data.zip'

#------------------------------------------------------------------------------
#
# 'experimentfile' input
#
#------------------------------------------------------------------------------
EXPERIMENTFILE_CASE,EXPERIMENTFILE_ID,EXPERIMENTFILE_FILEPATH = None,None,None # Default (don't modify)

# NB : 'experimentfile' taken into account and mandatory with
# ACTION_NAME value : 'post_vpz_experiment'

#> Choose (comment/uncomment) and define one case

# Case of existing dataset ----------------------------------------------------

#EXPERIMENTFILE_CASE = "EXISTING_DATASET"
#EXPERIMENTFILE_ID = '3b85fa1c3c38d39f' # must be an available value !!!

# Case of uploading a local file ----------------------------------------------

#EXPERIMENTFILE_CASE = "UPLOAD_FILE"

# EXPERIMENTFILE_FILEPATH must be an available value !!!
# Some files examples
#EXPERIMENTFILE_FILEPATH = '../data/experiment.xls'

###############################################################################
#
#              'erecord_file' TOOL OUTPUTS
#
# Outputs are : 'output_json' 
#             + maybe a second file ('experiment', 'conditions', 'report')
#
# Cases :
# - 'output_json' : download in memory, download as file
# -  second file  : download as file
#
#> Choose how to get outputs (True/False), several cases possible
#
###############################################################################

# 'output_json' case of downloading in memory
OUTPUT_JSON_DOWNLOAD_IN_MEMORY_CASE = True # values : True,False

# 'output_json' case of downloading as file
OUTPUT_JSON_DOWNLOAD_FILE_CASE = True # values : True,False

# second file case of downloading as file
SECOND_FILE_DOWNLOAD_FILE_CASE = True # values : True,False

###############################################################################

