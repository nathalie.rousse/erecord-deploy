# -*- coding: UTF-8 -*-
"""
    siwaa_erecord_json
"""

#import bioblend.galaxy
#import json
from pprint import pprint
import siwaa

PM = "[siwaa_erecord_json]"

class Siwaa_erecord_json(object):
    """Specific to 'erecord_json' tool"""

    def __init__(self, sc):

        self.sc = sc # SiwaaClient
        self.tool_id = 'erecord_json'
        if self.sc.V:
            print("Siwaa_erecord_json: Tool", self.tool_id)

    def set_tool_inputs(self, input_json_id, datafolder_id):
        """Define and return tool_inputs (parameter for run_tool)"""

        # cf .xml <inputs>

        tool_inputs = dict()

        # input_json
        if input_json_id is not None:
            tool_inputs['input_json'] = {'id':input_json_id, 'src':'hda'}

        # datafolder
        if datafolder_id is not None:
            tool_inputs['datafolder'] = {'id':datafolder_id, 'src':'hda'}

        return tool_inputs

    def run_tool(self, input_json_id=None, datafolder_id=None):
        """
        From erecord_json.xml :
        * inputs:
          - name='input_json' optional='true' type='data' format='json'
          - name='datafolder' optional='true' type='data" format='zip'
        * outputs:
          - name='output_json' format='json'
        """

        tool_inputs = self.set_tool_inputs(input_json_id=input_json_id,
                                           datafolder_id=datafolder_id)
        if self.sc.V:
            print("tool_inputs:")
            pprint(tool_inputs)
            print("run_tool with")
            print("'input_json' Dataset id :", input_json_id)
            print("'datafolder' Dataset id :", datafolder_id)
            print("...")

        ret = self.sc.tc.run_tool(history_id=self.sc.history_id,
                                  tool_id=self.tool_id,
                                  tool_inputs=tool_inputs)
        hda_output_json_id = self.sc.get_first_output_id(ret_run_tool=ret)
        return hda_output_json_id

