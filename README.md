# erecord-deploy

## Desc

Deployment about erecord project (erecord as python library).

## [Containers](./containers)

- Containers for some vle models that will be able to be used
  by erecord python library, directly as containers ('singularity exec...') or
  by Galaxy tools based on those containers
  (see galaxy-tools/erecord_json,erecord_file).

- Such a container content : vle, vle model, python, erecord python library.

- Containers written as Singularity recipes.

- [Use containers](./use-containers)

## [Galaxy tools](./galaxy-tools)

- Siwaa tools : erecord_json, erecord_file, erecord_text.

- [Use Galaxy tools](./use-tools)

# CI/CD

- CI/CD






