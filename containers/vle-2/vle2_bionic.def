###############################################################################
#
#                      Singularity recipe file for
#
#                    Container of vle (vle-2 version) + rvle + erecord
#
# Content :
#
#     - Ubuntu 18.04 (bionic)
#     - vle-2.0.2 version
#     - rvle_2.0.2-7 version
#     - R
#     - Python 3.5.3
#
#     - erecord (from https://forgemia.inra.fr/nathalie.rousse/erecord.git) :
#       - erecord vle model (main + dependencies packages).
#       - erecord Python library
#
#     - Apps : context, vle_version, vle_version_number,
#              simulate, pkg_list, vpz_list, data_list
#
# - Build container named VLE.simg :
#
#     sudo singularity build VLE.simg VLE.def
#
# - Help :
#
#     singularity run-help VLE.simg
#
# Use :
#
#     singularity exec VLE.simg vle --version
#     singularity exec VLE.simg python --version
#     singularity shell VLE.simg
#
###############################################################################

Bootstrap: library
From: ubuntu:18.04

%help

  Container of vle-2.0.2

  General :

  -- Launch the runscript (simulation) within container :

         singularity run VLE.simg pkgname vpzname

  -- Run a Bourne shell within container :

         singularity shell VLE.simg

  -- Execute a command within container, example :

          singularity exec VLE.simg vle --version

  Apps :
   
  -- List of apps :

         singularity inspect --list-apps VLE.simg

  -- Help for an app :

         singularity run-help --app appname VLE.simg

  -- Launch an app within container :

         singularity run --app appname VLE.simg

%post

  #############################################################################

  echo ""
  echo "###  Install vle-2.0.2 and its dependencies ###"
  echo ""

  # Dependencies
  apt-get -y update
  apt-get -y upgrade

  apt-get -y install wget git gettext vim \
                     libopenmpi-dev openmpi-bin \
                     curl libxml2-dev libboost-dev cmake pkg-config g++ \
                     qttools5-dev qttools5-dev-tools qtbase5-dev \
                     qtbase5-dev-tools qtchooser qt5-default libqt5svg5-dev

  ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime
  apt-get -y --reinstall install tzdata
  cat /etc/timezone

  apt-get -y clean

  mkdir /VLE
  mkdir /VLE/vle
  mkdir /VLE/vle_home
  mkdir /VLE/environment

  # VLE

  if ! [ -d /tmp/VLE ]
  then
      mkdir /tmp/VLE
  fi
  cd /tmp/VLE

  wget https://vle-project.org/pub/vle/2.0/2.0.2/vle-2.0.2.tar.gz --no-check-certificate
  if [ -d /tmp/VLE/vle-2.0.2 ]
  then
      rm -fr /tmp/VLE/vle-2.0.2
  fi
  tar zxf vle-2.0.2.tar.gz
  cd vle-2.0.2
  mkdir build
  cd build

  export QT_SELECT=qt5
  cmake -DCMAKE_INSTALL_PREFIX=/VLE/vle \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo -DWITH_CVLE=ON -DWITH_GVLE=ON ..
  make -j4
  make install
  #make test

  # Building vle.env
  vle_env_file=/VLE/environment/vle.env
  echo "export PATH=/VLE/vle/bin:$PATH " > $vle_env_file
  echo "export LD_LIBRARY_PATH=/VLE/vle/lib:$LD_LIBRARY_PATH " >> $vle_env_file
  echo "export MANPATH=/VLE/vle/man " >> $vle_env_file
  echo "export PKG_CONFIG_PATH=/VLE/vle/lib/pkgconfig " >> $vle_env_file
  echo "export VLE_HOME=/VLE/vle_home " >> $vle_env_file
  echo "export PKGS_FOLDER_NAME=pkgs-2.0 " >> $vle_env_file

  # Building vle.sh (not used)
  vle_sh_file=/VLE/environment/vle.sh
  echo "#!/bin/bash" > $vle_sh_file
  cat $vle_env_file >> $vle_sh_file
  chmod 777 $vle_sh_file

  # VLE_HOME
  VLE_ENV=$(cat $vle_env_file) && $VLE_ENV
  vle --restart
  chmod -R 777 $VLE_HOME

  # Nettoyage de temporaire
  cd /tmp
  rm -fr /tmp/VLE/vle-2.0.2 /tmp/VLE/vle-2.0.2.tar.gz

  echo ""
  echo "###  Install rvle_2.0.2-7 and R and Runit ###"
  echo ""

  apt-get install r-base -y
  apt-get install r-base-dev -y
  apt-get install r-cran-runit -y

  cd /tmp/VLE
  wget https://vle-project.org/pub/vle/2.0/2.0.2/rvle_2.0.2-7.tar.gz --no-check-certificate
  if [ -d /tmp/VLE/rvle ]
  then
      rm -fr /tmp/VLE/rvle
  fi
  tar zxf rvle_2.0.2-7.tar.gz
  R CMD build rvle
  R CMD INSTALL rvle

  # Nettoyage de temporaire
  cd /tmp
  rm -fr /tmp/VLE/rvle /tmp/VLE/rvle_2.0.2-7.tar.gz

  #############################################################################

  echo ""
  echo "###  Install erecord Python library ###"
  echo ""

  echo "Create /PYVENV Python Virtual Environment"

  apt-get -y install python3-pip python3-venv

  python3 -m venv /PYVENV
  . /PYVENV/bin/activate

  python3 -m pip install --upgrade pip wheel setuptools

  echo "Install erecord Python library into /PYVENV"
  echo "+ erecord Python code remained at /erecord"

  if ! [ -d /tmp/python_erecord ]
  then
      mkdir /tmp/python_erecord
  fi
  cd /tmp/python_erecord
  git clone https://forgemia.inra.fr/nathalie.rousse/erecord.git
  cp -fr erecord /erecord
  cd erecord
  python3 setup.py bdist_wheel
  #python3 -m pip install dist/erecord-0.0.1-py3-none-any.whl
  python3 -m pip install dist/erecord-*-py3-none-any.whl

  python3 -m pip list

  # Nettoyage de temporaire
  cd /tmp
  rm -fr /tmp/python_erecord

  #############################################################################

  echo ""
  echo "###  Install erecord and its dependencies vle packages ###"
  echo ""

  if [ -d /tmp/VLE/erecord ]
  then
      rm -fr /tmp/VLE/erecord
  fi
  cd /tmp/VLE
  git clone https://forgemia.inra.fr/nathalie.rousse/erecord.git
  cd /tmp/VLE/erecord/vle-pkgs/vle-2.0.0
  vle -P erecord configure build

  chmod -R 777 $VLE_HOME

  # Nettoyage de temporaire
  cd /tmp
  rm -fr /tmp/VLE/erecord

  apt-get -y clean

%environment

  # vle.env
  VLE_ENV=$(cat /VLE/environment/vle.env) && $VLE_ENV

%runscript

  echo "---------------------------------------------------------------------"
  echo "simulate $2 of $1"
  echo "---------------------------------------------------------------------"
  vle -P "$1" "$2"

# ===================================
# context
# ===================================

%apphelp context

  CALL : singularity run --app context VLE.simg

  DESC : Show context (information about vle, VLE_HOME, vle packages, vpz...)

%appenv context
  export PKG_LIST="$(cd $VLE_HOME/$PKGS_FOLDER_NAME; ls -d  */ | cut -f1 -d'/' | sed -e :a -e N -e 's/\n/ /' -e ta) "

%apprun context

  echo "---------------------------------------------------------------------"
  vle --version
  echo "---------------------------------------------------------------------"
  echo "VLE_HOME : $VLE_HOME"
  echo "---------------------------------------------------------------------"
  ##for pkg in $PKG_LIST; do
  ##  vle -P $pkg list
  ##done
  #echo "---------------------------------------------------------------------"
  echo "Installed vle packages : "
  echo $PKG_LIST
  echo "---------------------------------------------------------------------"
  echo "Installed vle packages and their vpz files : "
  for pkg in $PKG_LIST; do
    VPZ_LIST="$(vle -P $pkg list | grep /exp/ | sed 's/\/exp\//\&/' | cut -f2 -d'&' | sed -e :a -e N -e 's/\n/ /' -e ta)"
    echo "$pkg : $VPZ_LIST"
  done

  echo "---------------------------------------------------------------------"

# ===================================
# vle_version
# ===================================

%apphelp vle_version

  CALL : singularity run --app vle_version VLE.simg

  DESC : vle version information (full text)

%apprun vle_version
  vle --version

# ===================================
# vle_version_number
# =================================== 

%apphelp vle_version_number

  CALL : singularity run --app vle_version_number VLE.simg

  DESC : vle version number

%apprun vle_version_number
  vle --version | head -n 1 | sed -r 's/.*([0-9].[0-9].[0-9]).*/\1/'

# ===================================
# simulate
# ===================================

%apphelp simulate

  CALL : singularity run --app simulate VLE.simg pkgname vpzname

  DESC : Simulate vpzname vpz of pkgname package

  INPUTS :
      $1 : pkgname
      $2 : vpzname

%apprun simulate
  vle -P "$1" "$2"

# ===================================
# pkg_list
# ===================================

%apphelp pkg_list

  CALL : singularity run --app pkg_list VLE.simg

  DESC : List of installed packages

%appenv pkg_list
  export PKG_LIST="$(cd $VLE_HOME/$PKGS_FOLDER_NAME; ls -d  */ | cut -f1 -d'/' | sed -e :a -e N -e 's/\n/ /' -e ta) "

%apprun pkg_list
  echo $PKG_LIST

# ===================================
# vpz_list
# ===================================

%apphelp vpz_list

 a) For one package

    CALL : singularity run --app vpz_list VLE.simg pkgname

    DESC : List of vpz of pkgname package

    INPUT : 
       $1 : pkgname

 b) For all packages

    CALL : singularity run --app vpz_list VLE.simg 

    DESC : List of vpz of all packages

%appenv vpz_list
  export PKG_LIST="$(cd $VLE_HOME/$PKGS_FOLDER_NAME; ls -d  */ | cut -f1 -d'/' | sed -e :a -e N -e 's/\n/ /' -e ta) "

%apprun vpz_list
 
  if [ "$1" ]
  then
      pkg=$1
      if [ -d $VLE_HOME/$PKGS_FOLDER_NAME/$pkg ]
      then
        VPZ_LIST="$(vle -P $pkg list | grep /exp/ | sed 's/\/exp\//\&/' | cut -f2 -d'&' | sed -e :a -e N -e 's/\n/ /' -e ta)"
        echo "$VPZ_LIST"
      else
         echo "$pkg is not an installed package"
      fi
  else # all packages
      for pkg in $PKG_LIST; do
        VPZ_LIST="$(vle -P $pkg list | grep /exp/ | sed 's/\/exp\//\&/' | cut -f2 -d'&' | sed -e :a -e N -e 's/\n/ /' -e ta)"
        echo "$pkg : $VPZ_LIST"
      done
  fi

# ===================================
# data_list
# ===================================

%apphelp data_list

 CALL : singularity run --app data_list VLE.simg pkgname

 DESC : List of data files of pkgname package

 INPUT : 
    $1 : pkgname

%apprun data_list
 
  pkg=$1
  if [ -d $VLE_HOME/$PKGS_FOLDER_NAME/$pkg ]
  then
      if [ -d $VLE_HOME/$PKGS_FOLDER_NAME/$pkg/data ]
      then
        DATA_LIST="$(cd $VLE_HOME/$PKGS_FOLDER_NAME/$pkg/data; find . -type f | sed -e 's/\.\///' | sed -e :a -e N -e 's/\n/ /' -e ta)"
      else
        DATA_LIST=""
      fi
      echo "$DATA_LIST"
  else
     echo "$pkg is not an installed package"
  fi

# ===================================

