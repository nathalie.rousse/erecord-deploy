###############################################################################
#
#                      Singularity recipe file for
#
#               Container of vle packages in vle-2 version :
#
#                            (partial) recordb
#
# Content :
#   - vle (vle-2 version) and rvle
#   - vle packages in vle-2 version :
#     (partial) recordb models main+dependencies packages
#
# - Build container named REP.simg :
#
#   - Install vle : sudo singularity build VLE.simg VLE.def
#   - Add recordb : sudo singularity build REP.simg REP.def
#
# - Help :
#
#     singularity run-help REP.simg
#
###############################################################################

Bootstrap: localimage
From: vle2_bionic.simg

%help

  Container of vle-2.0.2 and rvle, (partial) recordb packages and dependencies packages with erecord package

  General :

  -- Launch the runscript (simulation) within container :

         singularity run REP.simg pkgname vpzname

     Example : singularity run REP.simg wwdm wwdm.vpz

  -- Run a Bourne shell within container :

         singularity shell REP.simg

  -- Execute a command within container, example :

         singularity exec REP.simg vle --version

         singularity exec REP.simg vle -P wwdm wwdm.vpz

  -- ALSO possible to use erecord package

  Apps :
   
  -- List of apps :

         singularity inspect --list-apps REP.simg

  -- Help for an app :

         singularity run-help --app appname REP.simg

  -- Launch an app within container :

        singularity run --app appname VLE.simg

     Examples :

       - Explore model :

         singularity run --app pkg_list REP.simg
         singularity run --app vpz_list REP.simg wwdm
         singularity run --app data_list REP.simg wwdm

       - Simulation :

         singularity run --app simulate REP.simg wwdm wwdm.vpz

%post

  # Note : /VLE /VLE/vle /VLE/vle_home /VLE/environment should exist

  # vle.env
  VLE_ENV=$(cat /VLE/environment/vle.env) && $VLE_ENV

  # Building rep.env
  rep_env_file=/VLE/environment/rep.env
  echo "" > $rep_env_file

  # Building rep.sh (not used)
  rep_sh_file=/VLE/environment/rep.sh
  echo "#!/bin/bash" > $rep_sh_file
  cat $rep_env_file >> $rep_sh_file
  chmod 777 $rep_sh_file

  #############################################################################
  #                         VLE model (packages)
  #############################################################################

  echo ""
  echo "###  Install wwdm (maybe more later) ###"
  echo ""

  if ! [ -d /tmp/VLE ]
  then
      mkdir /tmp/VLE
  fi

  if [ -d /tmp/VLE/recordb ]
  then
      rm -fr /tmp/VLE/recordb
  fi
  mkdir /tmp/VLE/recordb
  cd /tmp/VLE/recordb/

  # Install some vle packages
  if [ -d /tmp/VLE/recordb/packages ]
  then
      rm -fr /tmp/VLE/recordb/packages
  fi
  cd /tmp/VLE/recordb
  git clone https://github.com/vle-forge/packages.git
  cd packages
  git checkout master
  vle -P vle.reader configure build
  vle -P vle.tester configure build
  vle -P vle.discrete-time configure build

  # Install some recordb packages
  if [ -d /tmp/VLE/recordb/RECORD ]
  then
      rm -fr /tmp/VLE/recordb/RECORD
  fi
  cd /tmp/VLE/recordb
  git clone https://forgemia.inra.fr/record/RECORD.git
  cd RECORD
  git checkout master
  cd /tmp/VLE/recordb/RECORD/pkgs
  vle -P record.meteo configure build
  vle -P wwdm configure build
  vle -P DateTime configure build

  chmod -R 777 $VLE_HOME

  # Nettoyage de temporaire
  cd /tmp
  rm -fr /tmp/VLE/recordb

%environment

  # vle.env
  VLE_ENV=$(cat /VLE/environment/vle.env) && $VLE_ENV

  # rep.env
  REP_ENV=$(cat /VLE/environment/rep.env) && $REP_ENV

%runscript

  echo "---------------------------------------------------------------------"
  echo "simulate $2 of $1"
  echo "---------------------------------------------------------------------"
  vle -P "$1" "$2"

