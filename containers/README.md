# Containers

# Desc

Containers for some vle models to be used by 'erecord' python library.

Build process :

  - a 'vle' image built from a 'vle' .def :

    - the .def installs vle, 'erecord' python library, 'erecord' vle package.

    - there are 2 'vle' images built :

      - vle2_bionic.simg  for vle-2.0.2 (OS ubuntu 18.04)
      - vle1_stretch.simg for vle-1.1.3 (OS debian 9)

  - a 'rep' image built from 'rep' .def :

    - the 'rep' .def (FROM: 'vle' image) installs the model(s) vle packages.
    - there are several 'rep' images built, according to different models.

Model(s) source code : from erecord_deliv project (private).

Note : In case of several vle models installed into the container : since all
vle packages are installed under a unique VLE_HOME, be careful about packages
versions compatibility.

Note : Containers called/used by Galaxy tools (erecord_json, erecord_file,
erecord_text).

# vle-1

# vle1_stretch.simg : the 'vle' image for models in vle-1 version

- vle1_stretch.def (debian 9, vle-1.1.3) --> vle1_stretch.simg

- Build vle1_stretch.simg :

  ```bash
  sudo singularity build -F vle1_stretch.simg vle1_stretch.def
  ```

- Use vle1_stretch.simg :

  ```bash
  singularity exec vle1_stretch.simg python3 --version
  singularity exec vle1_stretch.simg vle --version
  singularity run --app pkg_list vle1_stretch.simg
  singularity run --app vpz_list vle1_stretch.simg
  singularity shell vle1_stretch.simg
  ```

# Model mastic (vle-1 version)

- rep_mastic.def (FROM: vle1_stretch.simg) --> rep_mastic.simg

- Build rep_mastic.simg :

  ```bash
  # if vle1_stretch.simg not ready
  sudo singularity build -F vle1_stretch.simg vle1_stretch.def
  
  sudo singularity build -F rep_mastic.simg rep_mastic.def
  ```

- Use rep_mastic.simg :

  ```bash
  singularity exec rep_mastic.simg python3 --version
  singularity exec rep_mastic.simg vle --version
  singularity run --app vpz_list rep_mastic.simg

  # mastic simulators
  singularity exec rep_mastic.simg vle -P RS_City City.vpz
  singularity exec rep_mastic.simg vle -P RS_FilRouge record_school.vpz
  singularity exec rep_mastic.simg vle -P RS_CropModel RS_CropModel.vpz
  singularity exec rep_mastic.simg vle -P RS_CropModel RS_CropModelR.vpz
  ```

# Model recordschool (vle-1 version)

- rep_recordschool.def (FROM: vle1_stretch.simg) --> rep_recordschool.simg

- Build rep_recordschool.simg :

  ```bash
  # if vle1_stretch.simg not ready
  sudo singularity build -F vle1_stretch.simg vle1_stretch.def

  sudo singularity build -F rep_recordschool.simg rep_recordschool.def
  ```

- Use rep_recordschool.simg :

  ```bash
  singularity exec rep_recordschool.simg python3 --version
  singularity exec rep_recordschool.simg vle --version
  singularity run --app vpz_list rep_recordschool.simg

  # recordschool simulators
  singularity exec rep_recordschool.simg vle -P RS_City City.vpz
  singularity exec rep_recordschool.simg vle -P RS_FilRouge record_school.vpz
  singularity exec rep_recordschool.simg vle -P RS_CropModel RS_CropModel.vpz
  singularity exec rep_recordschool.simg vle -P RS_CropModel RS_CropModelR.vpz
  ```

# Model solaid (vle-1 version)

- rep_solaid.def (FROM: vle1_stretch.simg) --> rep_solaid.simg

- Build rep_solaid.simg :

  ```bash
  # if vle1_stretch.simg not ready
  sudo singularity build -F vle1_stretch.simg vle1_stretch.def

  sudo singularity build -F rep_solaid.simg rep_solaid.def
  ```

- Use rep_solaid.simg :

  ```bash
  singularity exec rep_solaid.simg python3 --version
  singularity exec rep_solaid.simg vle --version
  singularity run --app vpz_list rep_solaid.simg

  # solaid simulators : none ready to verify
  ```

# vle-2

# vle2_bionic.simg : the 'vle' image for models in vle-2 version

- vle2_bionic.def (Ubuntu 18.04, vle-2.0.2) --> vle2_bionic.simg

-  Build vle2_bionic.simg :

  ```bash
  sudo singularity build -F vle2_bionic.simg vle2_bionic.def
  ```

- Use vle2_bionic.simg :

  ```bash
  singularity exec vle2_bionic.simg python3 --version
  singularity exec vle2_bionic.simg vle --version
  singularity run --app pkg_list vle2_bionic.simg
  singularity run --app vpz_list vle2_bionic.simg
  singularity shell vle2_bionic.simg
  ```

# Model recordb (vle-2 version)

- rep_recordb.def (FROM: vle2_bionic.simg) --> rep_recordb.simg

- Build rep_recordb.simg :

  ```bash
  # if vle2_bionic.simg not ready
  sudo singularity build -F vle2_bionic.simg vle2_bionic.def
  
  sudo singularity build -F rep_recordb.simg rep_recordb.def
  ```

- Use rep_recordb.simg :

  ```bash
  singularity exec rep_recordb.simg python3 --version
  singularity exec rep_recordb.simg vle --version
  singularity run --app vpz_list rep_recordb.simg

  # recordb simulators
  singularity exec rep_recordb.simg vle -P wwdm wwdm.vpz
  #singularity exec rep_recordb.simg vle -P 2CV 2CV-parcelle.vpz
  ```

# misc

```bash
singularity cache clean --dry-run
```

