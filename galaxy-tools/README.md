# galaxy-tools

## Desc

Galaxy tools for Siwaa : erecord_json, erecord_file, erecord_text.

Those tools are based on some containers from 'containers' folder, that
allow some vle models to be able to be used by erecord python library.

Each of those Galaxy tools may call directly one of the python main programs
that are included into 'erecord' python library (main_json.py, main_file.py),
or it may have its own python main program (derived from them).

See erecord project for 'doc'.


## Local test by 'planemo serve'

### Installs 

- Install Singularity : singularity version 3.8.5

- Install Galaxy code :
```
cd ~/DEVS
git clone https://github.com/galaxyproject/galaxy.git
```

Note : Galaxy requiring at least singularity 3.7
       (cf singularity exec : --no-mount option)

- Install Planemo into a Python virtualenv :
```
pip3 install --upgrade pip wheel setuptools virtualenv
python3 -m venv _venv_planemo
source _venv_planemo/bin/activate

python3 -m pip install planemo
```

### Use

```
source _fab/_venv_planemo/bin/activate
```

- Survey :
```
htop
```

- Check tool syntax :
```
cd tools
planemo lint --report_level all --fail_level error erecord_json/erecord_json.xml
```

- Run tool :
```
make serveERECORD
```

=> http://127.0.0.1:9090

