# -*- coding: UTF-8 -*-
""" 
    main_file

    Requests having as OUTput : 1 json data + maybe 1 more file (xls or zip)

    Accepts actions (all) :
    'help'
    'get_pkg_list' 'get_pkg_vpz_list' 'get_pkg_data_list'
    'get_vpz_input' 'post_vpz_input'
    'get_vpz_output' 'post_vpz_output'
    'get_vpz_inout' 'post_vpz_inout'
    'get_vpz_experiment' 'post_vpz_experiment'
    'get_vpz_report_conditions' 'post_vpz_report_conditions'
    'get_vpz_report' 'post_vpz_report'

    Parameters (Inputs) :
    - '-action' parameter : action value (mandatory)
    - '-data'   parameter : data (JSON format) (optional)
    - '-datafolder' parameter : datafolder file path (optional,
      available only in some cases)
    - '-experimentfile' parameter : experimentfile path (optional,
      available only in some cases) :

    If there is an action value into data, then this one is ignored.

    Outputs :
    - output.json data file (jsonoutput_filepath)
    - 1 other file in following cases : 
      - experiment.xls file
        if 'action' == get_vpz_experiment, post_vpz_experiment
      - conditions.xls file
        if 'action' == get_vpz_report_conditions,
                       post_vpz_report_conditions
      - report.zip file
        if 'action' == get_vpz_report, post_vpz_report
"""

MAIN_DEBUG=False # True for more information returned

import argparse
import json
import erecord.api.erecord as erecord_api
from erecord.api.cmn.views_mixins import ActionViewMixin
from erecord.api.cmn.utils.errors import build_error_content

import os
import shutil

PM = "[main_file.py]"

r = dict()

import sys
if MAIN_DEBUG: r["sys.path"] = sys.path # debug
if MAIN_DEBUG: r["sys.argv"] = sys.argv # debug

data = dict()
json_response = dict()

# default configuration + maybe some modifications
config = erecord_api.conf() # default (vle-2.0.0 version)
config.DB_add_vpz(vpz_id=1312, vpzname="wwdm.vpz", pkgname="wwdm")

try :
    parser = argparse.ArgumentParser()

    # -action
    available_action_list = ActionViewMixin.get_action_list()
    htext = "action value among : " + str(available_action_list)
    parser.add_argument("-action", default='help', help=htext,
                        choices=available_action_list)

    # -data
    parser.add_argument("-data", type=str, help="data json file path")

    # -datafolder
    htext = "datafolder file path, available for action : "
    htext +="post_vpz_output, post_vpz_inout,"
    htext +="post_vpz_experiment, post_vpz_report"
    parser.add_argument("-datafolder", type=str, help=htext)

    # -experimentfile
    htext = "experimentfile path, available for action : post_vpz_experiment"
    parser.add_argument("-experimentfile", type=str, help=htext)

    args = parser.parse_args()

    if (args.action is None) or (args.action == 'None') :
        warn = "NO action given => enforced to 'help'"
        r["warning"] = warn
        action = 'help' # enforced (default)
    else : 
        action = args.action

        if (args.data is None) or (args.data == 'None') :
            data = dict() # NO data (JSON format) given as argument
        else : 
            jsoninput_filename = args.data
            inputfile = open(jsoninput_filename, 'r')
            data = json.load(inputfile) # get data

        if (args.datafolder is None) or (args.datafolder == 'None') :
            pass # NO datafolder given
        else :
            data['datafolder'] = args.datafolder

        if (args.experimentfile is None) or (args.experimentfile == 'None') :
            pass # NO experimentfile given
        else :
            data['experimentfile'] = args.experimentfile

    data['action'] = action

    r["data"] = data

    # configuration
    if MAIN_DEBUG: r["config"] = config.get_as_dict() # debug

    r_init = erecord_api.init(config)
    if MAIN_DEBUG: r["init"] = r_init # debug

    response = erecord_api.action(config, data)
    json_response = json.loads(response)

    # Restitution : file (experiment.xls, conditions.xls, report.zip)
    # Note :
    #   tool.xml command "touch" the file (in case of failed to be copied here)
    #   for it to exist (even if empty) => able to be found
    if action in ['get_vpz_experiment', 'post_vpz_experiment',
                  'get_vpz_report_conditions', 'post_vpz_report_conditions',
                  'get_vpz_report', 'post_vpz_report']:
        try:
            filename = json_response["outputfile_name"]
            outputfile_path = json_response["outputfile_path"]
            shutil.copyfile(src=outputfile_path,
                            dst=os.path.join(config.out_path,filename))
                                                          # tool.xml uses them

            if not MAIN_DEBUG: # some response information not kept
                for k in ('outputfile_name', 'outputfile_path', 'zip_file_path'):
                    if k in json_response.keys(): del(json_response[k])

        except Exception as e :

            msg = PM+"[second file "
            if "outputfile_name" in json_response.keys() :
                msg = "('"+ json_response["outputfile_name"] + "')"
            msg = " restitution]"
            cnt = build_error_content(exception=e, errormsg=msg)
            json_response["error PLUS"] = str(cnt)

except Exception as e :

    response = erecord_api.error_case(data=data, msg=PM, e=e)
    json_response = json.loads(response)

r["response"] = json_response

try :
    # Restitution : json file
    jsonoutput_filepath = os.path.join(config.out_path,
                                       config.jsonoutput_filename)
    with open(jsonoutput_filepath, "w") as outfile:
        json.dump(r, outfile)
except :
    print(PM, " -- error when trying to open output json file --")

