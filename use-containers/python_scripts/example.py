# -*- coding: UTF-8 -*-
""" 
    example

    Requests for wwdm.vpz of wwdm pkg : 'get_vpz_input' 'post_vpz_output'

"""

import erecord.api.erecord as erecord_api
import json
import pprint

config = erecord_api.conf()

data = dict()
json_response = dict()

try :
    data1 = {"action": "get_vpz_input",
            "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist"
           }

    data2 = {"action": "get_vpz_input",
            "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist",
            "parselect": ["cond_meteo.meteo_file", "cond_wwdm",
                          "simulation_engine" ]
           }

    data3 = {"action":"post_vpz_output",
            "pkgname":"wwdm", "vpzname": "wwdm.vpz", "style": "compactlist",
            "plan": "single", "restype": "dataframe", 
            "duration": 10.0}

    data4 = {"action": "post_vpz_output",
            "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist",
            "plan": "linear", "restype": "dataframe", 
            "cond_wwdm.A": [0.0063, 0.0065, 0.0067], "cond_wwdm.B": 0.00201, 
            "duration": 6.0, 
            "outselect": "view.top:wwdm.LAI"
           }

    erecord_api.init(config)

    data = data1
    response = erecord_api.action(config, data)

    json_response = json.loads(response)

except Exception as e :

    response = erecord_api.error_case(data=data, msg="[example]", e=e)

    json_response = json.loads(response)

print("")
print("Request data :")
print("")
pprint.pprint(data)
print("")
print("Response :")
print("")
pprint.pprint(json_response)
print("")
