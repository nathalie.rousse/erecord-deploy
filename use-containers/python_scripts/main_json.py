# -*- coding: UTF-8 -*-
""" 
    main_json

    Requests having only 1 json data as OUTput

    Accepts actions :
    'get_vpz_input' 'post_vpz_input'
    'get_vpz_output' 'post_vpz_output'
    'get_vpz_inout' 'post_vpz_inout'
    'get_pkg_list' 'get_pkg_vpz_list' 'get_pkg_data_list'
    'help'

    Refused actions :
    'get_vpz_experiment' 'post_vpz_experiment'
    'get_vpz_report_conditions' 'post_vpz_report_conditions'
    'get_vpz_report' 'post_vpz_report'

    Parameters (Inputs) :
    - '-data' parameter : data (JSON format) (optional)
      A required action value must be included into data.
    - '-datafolder' parameter : datafolder file path (optional,
      available only in some cases)

   Outputs :
   - output.json data file (jsonoutput_filepath)
"""

MAIN_DEBUG=False # True for more information returned

import argparse
import json
import erecord.api.erecord as erecord_api

import os

PM = "[main_json.py]"

UNACCEPTED_ACTIONS_LIST = ['get_vpz_experiment', 'post_vpz_experiment',
                   'get_vpz_report_conditions', 'post_vpz_report_conditions',
                   'get_vpz_report', 'post_vpz_report']

r = dict()

import sys
if MAIN_DEBUG: r["sys.path"] = sys.path # debug
if MAIN_DEBUG: r["sys.argv"] = sys.argv # debug

data = dict()
json_response = dict()

# default configuration + maybe some modifications
config = erecord_api.conf() # default (vle-2.0.0 version)
config.DB_add_vpz(vpz_id=1312, vpzname="wwdm.vpz", pkgname="wwdm")

try :
    parser = argparse.ArgumentParser()
    parser.add_argument("-data", type=str, help="data json file path")
    parser.add_argument("-datafolder", type=str, help="datafolder file path")

    args = parser.parse_args()

    if (args.data is None) or (args.data == 'None') :
        warn = "NO data (JSON format) given => action enforced to 'help'"
        r["warning"] = warn
        data = {'action':'help'} # enforced (default)
    else:
        jsoninput_filename = args.data
        inputfile = open(jsoninput_filename, 'r')
        data = json.load(inputfile) # get data

        if (args.datafolder is None) or (args.datafolder == 'None') :
            pass # NO datafolder given
        else :
            data['datafolder'] = args.datafolder

    # Pre-filtering : Unaccepted actions => enforced to 'help' action
    if "action" in data.keys() :
        a = data["action"]
        if a in UNACCEPTED_ACTIONS_LIST :
            warn = "Unaccepted action '"+a+"' => action enforced to 'help'"
            r["warning"] = warn
            data["action"] = 'help'

    r["data"] = data

    # configuration
    if MAIN_DEBUG: r["config"] = config.get_as_dict() # debug

    r_init = erecord_api.init(config)
    if MAIN_DEBUG: r["init"] = r_init # debug

    response = erecord_api.action(config, data)
    json_response = json.loads(response)

except Exception as e :

    response = erecord_api.error_case(data=data, msg=PM, e=e)
    json_response = json.loads(response)

r["response"] = json_response

try :
    # Restitution : json file
    jsonoutput_filepath = os.path.join(config.out_path,
                                       config.jsonoutput_filename)
    with open(jsonoutput_filepath, "w") as outfile:
        json.dump(r, outfile)

except :
    pass
