#!/bin/bash

#
# Command : /bin/bash ./cmd_erecord_json.sh input_json datafolder output_json 
#
# - 3 parameters (path files) :
#
#   - 1st param : input_json path file
#     (that it use as -data argument of main_json)
#
#   - 2nd param : datafolder path file
#     (that it uses as -datafolder argument of main_json)
#
#     datafolder value "None" if no datafolder
#
#   - 3rd param : output_json path file
#     (where it copies WS/OUT/output.json output json file)
#
#     output_json value "None" for screen display
#

input_json=$1 
datafolder=$2
output_json=$3

. /PYVENV/bin/activate;
/bin/bash -c "python3 /erecord/erecord/main_json.py -data ${input_json} -datafolder ${datafolder}";


if [[ "${output_json}" == "None" ]]
then
    cat /WS/OUT/output.json;
else
    cp WS/OUT/output.json ${output_json};
fi


