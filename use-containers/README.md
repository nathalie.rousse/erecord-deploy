# Using containers to run a python program that uses 'erecord' python library

# Container

Containers from '[containers](../containers)' folder, that allow
some vle models to be able to be used by erecord python library.

Examples of containers :

 ```bash
  WWDM_SIMG_PATH=/home/nrousse/workspace_git/SIWAA_regroup/erecord-deploy/containers/_fab_SIMG/rep_recordb.simg

  # While development (erecord python library being modified), container :
  WWDM_SIMG_PATH=/home/nrousse/workspace_git/SIWAA_regroup/erecord-deploy/containers/vle-2/REP_updated.simg

  ```

Container content :

  - erecord python library installed in /PYVENV Python Virtual Environment.

  - the vle model, and also the required erecord vle package,
    installed under $VLE_HOME
 
  - Note : erecord python library source code can be seen under /erecord 

# Calling the container

Basic calls :
  - ```singularity run-help ...```
  - ```singularity inspect --list-apps ...```
  - ```singularity run --app {appname} ...```
  - ```singularity exec ...```

Examples :

  ```bash
  # help
  singularity run-help $WWDM_SIMG_PATH

  singularity run --app context $WWDM_SIMG_PATH
  singularity exec $WWDM_SIMG_PATH python3 --version
  singularity exec $WWDM_SIMG_PATH vle --version
  singularity run --app vle_version_number $WWDM_SIMG_PATH

  # List of 'singularity' apps
  singularity inspect --list-apps $WWDM_SIMG_PATH

  # models
  singularity run --app pkg_list $WWDM_SIMG_PATH
  singularity run --app vpz_list $WWDM_SIMG_PATH

  # model
  singularity run --app vpz_list $WWDM_SIMG_PATH wwdm
  singularity run --app data_list $WWDM_SIMG_PATH wwdm

  # simulation
  singularity run $WWDM_SIMG_PATH wwdm wwdm.vpz
  singularity run --app simulate $WWDM_SIMG_PATH wwdm wwdm.vpz
  singularity exec $WWDM_SIMG_PATH vle -P wwdm wwdm.vpz

  ```

To be able to use erecord python library :

  - a mount is required for "/WS" :

    ```singularity exec``` with option ```-B ./WS:/WS```

  - the "/PYVENV" Python virtual environment will have to be activated :

    ```bash
    . /PYVENV/bin/activate;
    ```

# erecord python library

erecord python library documentation : see 'doc' folder of erecord project. 

erecord python library content : 

  - /erecord/erecord/api

  - 2 python main programs : /erecord/erecord/main_json.py,main_file.py

It is possible to call one of the existing python main programs : 
/erecord/erecord/main_json.py,main_file.py ...

It is also possible to write ones own personalized python main programs
from the existing ones /erecord/erecord/main_json.py,main_file.py :
python_scripts/main_json.py,example.py ...

erecord main_json.py program :

  - Inputs :
    - argument -data : data json file path
    - argument -datafolder : datafolder file path

  - Outputs :
    - WS/OUT/output.json output json file
    - Note : some other things are accessible into WS folder.

erecord main_file.py program :

  - Inputs :
    - argument -action : 'action' value
    - argument -data : data json file path
    - argument -datafolder : datafolder file path
    - argument -experimentfile : experiment.xls file path

  - Outputs (one .json file + maybe one more file .zip or .xls) :
    - WS/OUT/output.json output json file (always)
    - WS/OUT/experiment.xls,conditions.xls,report.zip file
      (according to 'action').
    - Note : some other things are accessible into WS folder.

# Calls as shell commands

- Run erecord main_json.py program by cmd_erecord_json.sh (calling 'python /erecord/erecord/main_json.py ...') :

  ```bash
  # 'help'
  echo '{"action":"help"}' > mydata.json
  singularity exec -B ./WS:/WS $WWDM_SIMG_PATH /bin/bash ./shell_scripts/cmd_erecord_json.sh "mydata.json" "None" "None"

  # 'get_pkg_list'
  echo '{"action":"get_pkg_list"}' > mydata.json
  singularity exec -B ./WS:/WS $WWDM_SIMG_PATH /bin/bash ./shell_scripts/cmd_erecord_json.sh "mydata.json" "None" "myoutput.json" 

  # 'get_pkg_vpz_list'
  echo '{"action":"get_pkg_vpz_list", "pkgname":"wwdm"}' > mydata.json
  singularity exec -B ./WS:/WS $WWDM_SIMG_PATH /bin/bash ./shell_scripts/cmd_erecord_json.sh "mydata.json" "None" "myoutput.json" 

  # 'get_pkg_data_list'
  echo '{"action":"get_pkg_data_list", "pkgname":"wwdm"}' > mydata.json
  singularity exec -B ./WS:/WS $WWDM_SIMG_PATH /bin/bash ./shell_scripts/cmd_erecord_json.sh "mydata.json" "None" "myoutput.json" 

  # 'get_vpz_input'
  echo '{"action":"get_vpz_input", "pkgname":"wwdm", "vpzname":"wwdm.vpz"}' > mydata.json
  singularity exec -B ./WS:/WS $WWDM_SIMG_PATH /bin/bash ./shell_scripts/cmd_erecord_json.sh "mydata.json" "None" "myoutput.json"

  # 'post_vpz_output'
  echo '{ "action": "post_vpz_output", "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist", "plan": "linear", "restype": "dataframe", "cond_wwdm.A": [0.0063, 0.0065, 0.0067], "cond_wwdm.B": 0.00201, "duration": 6.0, "outselect": "view.top:wwdm.LAI" }' > mydata.json
  singularity exec -B ./WS:/WS $WWDM_SIMG_PATH /bin/bash ./shell_scripts/cmd_erecord_json.sh "mydata.json" "None" "myoutput.json"

  # 'post_vpz_output' with 'datafoldercopy' valuing 'overwrite' because using '31035002_bis.csv' file (from ../data/data.zip) and still using '31035002.csv' (from original data folder of "wwdm" simulator)

  echo '{ "action": "post_vpz_output", "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist", "plan": "linear", "restype": "matrix", "cond_meteo.meteo_file": ["31035002.csv", "31035002_bis.csv"], "datafoldercopy": "overwrite", "duration": 6.0, "outselect": "view.top:wwdm.LAI" }' > mydata.json

  singularity exec -B ./WS:/WS $WWDM_SIMG_PATH /bin/bash ./shell_scripts/cmd_erecord_json.sh "mydata.json" "../data/data.zip" "myoutput.json"

  ```

- Run erecord main_file.py program by cmd_erecord_file.sh (calling 'python /erecord/erecord/main_file.py ...') :

  ```bash
  TODO

  ```

- Run ones own example.py program by cmd_example.sh (calling 'python ./python_scripts/example.py ...') :

  ```bash
  singularity exec -B ./WS:/WS $WWDM_SIMG_PATH /bin/bash ./shell_scripts/cmd_example.sh
  ```

# Calls from R code

  R code with 'system' and files creation :

  ```R
  data = '{ "action": "post_vpz_output", "pkgname": "wwdm", "vpzname": "wwdm.vpz", "style": "compactlist", "plan": "linear", "restype": "dataframe", "cond_wwdm.A": [0.0063, 0.0065, 0.0067], "cond_wwdm.B": 0.00201, "duration": 6.0, "outselect": "view.top:wwdm.LAI" }' 

  cat(data, file="myRdata.json")

  # 'post_vpz_output'
  system('singularity exec -B ./WS:/WS /home/nrousse/workspace_git/SIWAA_regroup/erecord-deploy/containers/_fab_SIMG/rep_recordb.simg /bin/bash ./shell_scripts/cmd_erecord_json.sh "myRdata.json" "None" "myRoutput.json"')

  # read json data into myRoutput.json
  library(jsonlite)
  output_json_data <- fromJSON(txt = "myRoutput.json")
  output_json_data

  ```

