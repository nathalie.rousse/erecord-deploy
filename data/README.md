# data

Files as input
for 'erecord' python library main programs (main_json.py, main_file.py),
for 'erecord' Galaxy tools based on them (erecord_json', 'erecord_file',
'erecord_text').

'erecord_text' tool is concerned only by files for requests having and using
'datafolder' input.

Some of those files are available for 'erecord_json' tool : they contain "action" value ; they are also available for 'erecord_file' tool (in that case, the contained "action" value will be ignored).

Some other files are available only for 'erecord_file' tool : they do not contain "action" value (that is required in 'erecord_json' tool case).

- data__help.json
  - for request : "help"
  - includes "action" (required by 'erecord_json', ignored by 'erecord_file')

- data__get_pkg_data_list.json
  - for request : "get_pkg_data_list"
  - includes "action"

- data__get_pkg_list.json
  - for request : "get_pkg_list"
  - includes "action"

- data__get_pkg_vpz_list_2.json
  - for request : "get_pkg_vpz_list"
  - includes "action"

- data__get_pkg_vpz_list.json
  - for request : "get_pkg_vpz_list"
  - includes "action"

- data__get_vpz_input.json :
  - for request "get_vpz_input"
  - includes "action"

- data__get_vpz_output.json :
  - for request "post_vpz_input"
  - includes "action"

- data__post_vpz_output_2.json :
  - for request "post_vpz_output"
  - includes "action"
  - with "datafoldercopy":"overwrite" (used if there is a 'datafolder' input)

- data__post_vpz_output_3.json :
  - for request "post_vpz_output"
  - includes "action"
  - with "datafoldercopy":"replace" (used if there is a 'datafolder' input)

- data__post_vpz_inout.json :
  - for request "post_vpz_inout"
  - includes "action"

- data__vpz.json :
  - minimal data content for requests :
    - "get_vpz_input", "post_vpz_input",
    - "get_vpz_output", "post_vpz_output",
    - "get_vpz_inout", "post_vpz_inout",
    - "get_vpz_experiment",
    - "post_vpz_experiment" (requiring also experiment.xls file),
    - "get_vpz_report", "post_vpz_report",
    - "get_vpz_report_conditions", "post_vpz_report_conditions".
  - does not include "action" (=> only for 'erecord_file')

- data__post_vpz_report.json :
  - for request : "post_vpz_report" (available only for 'erecord_file')

- data__post_vpz_experiment.json :
  - for request : "post_vpz_experiment" (available only for 'erecord_file')

- experiment.xls : for request "post_vpz_experiment"
- data.zip : for requests having and using 'datafolder' input 

